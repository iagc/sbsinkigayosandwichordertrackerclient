﻿using Android.Content.PM;
using ShoppingCarts.Helpers.Interface;

[assembly: Xamarin.Forms.Dependency(typeof(ShoppingCarts.Droid.Version_Android))]
namespace ShoppingCarts.Droid
{
    public class Version_Android : IAppVersion
    {
        public string GetVersion()
        {
            var context = global::Android.App.Application.Context;

            PackageManager manager = context.PackageManager;
            PackageInfo info = manager.GetPackageInfo(context.PackageName, 0);

            return info.VersionName;
        }

        public int GetBuild()
        {
            var context = global::Android.App.Application.Context;
            PackageManager manager = context.PackageManager;
            PackageInfo info = manager.GetPackageInfo(context.PackageName, 0);

            return info.VersionCode;
        }
    }
}