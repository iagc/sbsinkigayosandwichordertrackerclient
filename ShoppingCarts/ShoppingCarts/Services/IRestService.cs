﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ShoppingCarts.Model;

namespace ShoppingCarts.Services
{
    public interface IRestService
    {
        Task<UserInfo> LoginInServer(string userId, string password);
    }
}
