﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Services
{
    public interface ICloseable
    {
        void Close();
    }
}
