﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ShoppingCarts.Model;
using MvvmHelpers;

namespace ShoppingCarts.Services.ServiceInterface
{
    public interface IPromoSelectionService
    {
        Task<List<PromoSummary>> GetPromoList();
        //Task<double> GetDiscountAmount(string id, double TotalAmount);
    }
}
