﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ShoppingCarts.Model;
using MvvmHelpers;

namespace ShoppingCarts.Services.ServiceInterface
{
    public interface ITransactionTypeService
    {
        Task<List<TransactionType>> GetTransactionTypes();
    }
}
