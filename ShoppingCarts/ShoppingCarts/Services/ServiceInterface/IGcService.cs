﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ShoppingCarts.Model;

namespace ShoppingCarts.Services.ServiceInterface
{
    public interface IGcService
    {
        Task<GiftCertificate> GetGiftCertificateTask(string GcNumber);
        Task<bool> RedeemGiftCertificateTask(string GcNumber);
    }
}
