﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;

namespace ShoppingCarts.Services.ServiceInterface
{
    public interface ISalesService
    {
        Task<List<SalesSummary>> GetSalesTask(int position, int pageSize, DateTime transactionDate, Constants.OrderStatus orderStatus);
        Task<SalesResult> PostSalesTask(SaleModel sale);
        Task<bool> PutSalesTask(string salesInvoiceNumber, int action);
        Task<SalesResult> PutSalesPaymentTask(string salesInvoiceNumber, SaleModel sale);
    }
}
