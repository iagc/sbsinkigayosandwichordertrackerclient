﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ShoppingCarts.Model;
using MvvmHelpers;

namespace ShoppingCarts.Services.ServiceInterface
{
    public interface IEmployeeListService
    {
        Task<List<CrpModel>> GetCRPList(string term);

        Task<List<EmployeeFoodAllowanceModel>> GetEmployeeFoodAllowanceList(string term);
    }
}
