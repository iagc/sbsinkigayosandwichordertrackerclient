﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ShoppingCarts.Model;
using MvvmHelpers;

namespace ShoppingCarts.Services.ServiceInterface
{
    public interface IPosBillService
    {
        Task<List<PosBillDetails>> GetPosBillDetails(string storeCode, string hostName, string terminalNumber);

        Task<bool> PostPosBillTask(string requestUrl, PosBillSummary posBill);
    }
}
