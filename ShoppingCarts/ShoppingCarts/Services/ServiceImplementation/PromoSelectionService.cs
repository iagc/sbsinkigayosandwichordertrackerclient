﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using MvvmHelpers;
using ShoppingCarts.Services.ServiceInterface;

namespace ShoppingCarts.Services.ServiceImplementation
{
    public class PromoSelectionService : IPromoSelectionService
    {
        public List<PromoSummary> PromoList;
        public HttpManager HttpManager;

        public PromoSelectionService()
        {
            PromoList = new List<PromoSummary>();
            HttpManager = new HttpManager();
        }

        public async Task<List<PromoSummary>> GetPromoList()
        {
            var response = await HttpManager.GetAsync<PromoSummary>(Constants.PromoItemURL());
            return response;
        }
    }
}
