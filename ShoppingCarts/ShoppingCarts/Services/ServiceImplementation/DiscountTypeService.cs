﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using MvvmHelpers;
using ShoppingCarts.Services.ServiceInterface;

namespace ShoppingCarts.Services.ServiceImplementation
{
    public class DiscountTypeService : IDiscountTypeService
    {
        public List<DiscountType> DiscountTypes;
        public HttpManager HttpManager;

        public DiscountTypeService()
        {
            DiscountTypes = new List<DiscountType>();
            HttpManager = new HttpManager();
        }

        public async Task<List<DiscountType>> GetDiscountTypes()
        {

            var response = await HttpManager.GetAsync<DiscountType>(Constants.DiscountTypesURL());
            return response;
        }

        public async Task<double> GetDiscountAmount(string id, double totalamount)
        {
            double response = await HttpManager.GetAsyncDouble($"{Constants.DiscountTypesURL()}/{id}/{totalamount}");
            return response;
        }
    }
}
