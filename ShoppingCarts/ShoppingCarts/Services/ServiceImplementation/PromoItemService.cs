﻿using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceInterface;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http;
using System;

[assembly: Dependency(typeof(ShoppingCarts.Services.ServiceImplementation.PromoItemService))]

namespace ShoppingCarts.Services.ServiceImplementation
{
    public class PromoItemService : IPromoItemService
    {
        public List<ProductDetail> ProductDetails;
        public HttpManager HttpManager;

        public PromoItemService()
        {
            ProductDetails = new List<ProductDetail>();
            HttpManager = new HttpManager();
        }

        public async Task<PromoSummary> GetPromoItems(string promoid)
        {
            var client = new HttpClient();
            var response = await client.GetAsync($"{Constants.PromoItemURL()}/{promoid}");

            if (!response.IsSuccessStatusCode)
            {
                var responseMessage = await response.Content.ReadAsStringAsync();
                throw new Exception(responseMessage);
            }

            var responseJson = await response.Content.ReadAsStringAsync();
            var jsonObject = JsonConvert.DeserializeObject<PromoSummary>(responseJson);
            return jsonObject;

        }
    }
}
