﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using MvvmHelpers;
using ShoppingCarts.Services.ServiceInterface;

namespace ShoppingCarts.Services.ServiceImplementation
{
    public class PromoItemSelectionService : IPromoItemSelectionService
    {
        

        public List<PromoItemSelectionModel> PromoItemList;
        public HttpManager HttpManager;

        public PromoItemSelectionService()
        {
            PromoItemList = new List<PromoItemSelectionModel>();
            HttpManager = new HttpManager();
        }

        public async Task<List<PromoItemSelectionModel>> GetPromoItemsSelection(string promoid)
        {
            var response = await HttpManager.GetAsync<PromoItemSelectionModel>($"{Constants.PromoItemSelectionURL()}/{promoid}");
            return response;
        }
    }
}
