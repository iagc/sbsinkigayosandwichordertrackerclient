﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using MvvmHelpers;
using ShoppingCarts.Services.ServiceInterface;

//[assembly: Dependency(typeof(ShoppingCarts.Services.ServiceImplementation.POSBillService))]

namespace ShoppingCarts.Services.ServiceImplementation
{
    public class PosBillService : IPosBillService
    {
        public List<PosBillDetails> PosBillDetails;
        public HttpManager HttpManager;

        public PosBillService()
        {
            PosBillDetails = new List<PosBillDetails>();
            HttpManager = new HttpManager();
        }

        public async Task<List<PosBillDetails>> GetPosBillDetails(string storeCode,string hostName,string terminalNumber)
        {
            var summary = await HttpManager.GetSingleAsync<PosBillSummary>($"{Constants.SavePosBillUrl()}/{storeCode}/{hostName}/{terminalNumber}");
            var response = summary.PosBillAppDetails;
            return response;
        }

        public async Task<bool> PostPosBillTask(string requestUrl, PosBillSummary posBill)
        {

            var client = new System.Net.Http.HttpClient();
            var jsonObject = JsonConvert.SerializeObject(posBill);
            var jsonContent = new StringContent(jsonObject, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(requestUrl, jsonContent);
            return response.IsSuccessStatusCode;       

        }
    }
}
