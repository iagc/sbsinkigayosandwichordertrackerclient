﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShoppingCarts.Model;

namespace ShoppingCarts.Services.ServiceImplementation
{
    public interface IItemService
    {
        Task<List<Item>> GetItems();
    }
}