﻿using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceInterface;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(ShoppingCarts.Services.ServiceImplementation.ProductDetailService))]

namespace ShoppingCarts.Services.ServiceImplementation
{
    public class ProductDetailService : IProductDetailService
    {
        public List<ProductDetail> ProductDetails;
        public HttpManager HttpManager;

        public ProductDetailService()
        {
            ProductDetails = new List<ProductDetail>();
            HttpManager = new HttpManager();
        }

        public async Task<List<ProductDetail>> GetProducts()
        {
            var response = await HttpManager.GetAsync<ProductDetail>(Constants.ProductUrl());
            return response;
        }
    }
}