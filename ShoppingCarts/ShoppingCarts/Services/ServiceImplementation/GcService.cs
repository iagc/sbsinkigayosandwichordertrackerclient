﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceInterface;

namespace ShoppingCarts.Services.ServiceImplementation
{
    public class GcService : IGcService
    {
        public async Task<GiftCertificate> GetGiftCertificateTask(string gcNumber)
        {
            var client = new System.Net.Http.HttpClient();
            var response = await client.GetAsync($"{Constants.GcUrl()}/{gcNumber}");

            if (!response.IsSuccessStatusCode)
            {
                var responseMessage = await response.Content.ReadAsStringAsync();
                throw new Exception(responseMessage);
            } 

            var responseJson = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<GiftCertificate>(responseJson);
            return result;

        }

        public async Task<bool> RedeemGiftCertificateTask(string gcNumber)
        {
            var client = new System.Net.Http.HttpClient();
            var jsonObject = JsonConvert.SerializeObject(new { Kamote = "Test" });
            var jsonContent = new StringContent(jsonObject, Encoding.UTF8, "application/json");
            var response = await client.PutAsync($"{Constants.GcUrl()}/{gcNumber}", jsonContent);
            return response.IsSuccessStatusCode;
        }
    }
}
