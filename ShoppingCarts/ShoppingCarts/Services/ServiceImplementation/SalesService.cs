﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceInterface;

namespace ShoppingCarts.Services.ServiceImplementation
{
    public class SalesService : ISalesService
    {
        public List<SalesSummary> Sales;
        public HttpManager HttpManager;

        public SalesService()
        {
            Sales = new List<SalesSummary>();
            HttpManager = new HttpManager();
        }

        public async Task<List<SalesSummary>> GetSalesTask(int position, int pageSize, DateTime transactionDate, Constants.OrderStatus orderStatus)
        {
            var response = await HttpManager.GetAsync<SalesSummary>($"{Constants.SalesUrl()}/{position}/{pageSize}/{transactionDate.ToString("yyyy-MM-dd")}/{orderStatus.ToString()}");
            return response;
        }

        public async Task<SalesResult> PostSalesTask(SaleModel sale)
        {

            var client = new HttpClient();
            var jsonObject = JsonConvert.SerializeObject(sale);
            var jsonContent = new StringContent(jsonObject, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(Constants.SalesUrl(), jsonContent);

            if (response.IsSuccessStatusCode)
            {
                var responseJson = await response.Content.ReadAsStringAsync();
                var j = JsonConvert.DeserializeObject<SalesResult>(responseJson);
                return j;
            }
            else
            {
                var responseMessage = await response.Content.ReadAsStringAsync();
                var x = JsonConvert.DeserializeObject<ErrorHelper>(responseMessage);
                throw new Exception(x.ExceptionMessage);
            }

        }

        public async Task<bool> PutSalesTask(string salesInvoiceNumber, int action)
        {
            var client = new HttpClient();                        
            var response = await client.PutAsync($"{Constants.SalesUrl()}/{salesInvoiceNumber}/{action}", null);
            return response.IsSuccessStatusCode;
        }

        public async Task<SalesResult> PutSalesPaymentTask(string salesInvoiceNumber, SaleModel sale)
        {
            var client = new HttpClient();
            var jsonObject = JsonConvert.SerializeObject(sale);
            var jsonContent = new StringContent(jsonObject, Encoding.UTF8, "application/json");
            var response = await client.PutAsync($"{Constants.SalesUrl()}/{salesInvoiceNumber}", jsonContent);

            if (response.IsSuccessStatusCode)
            {
                var responseJson = await response.Content.ReadAsStringAsync();
                var j = JsonConvert.DeserializeObject<SalesResult>(responseJson);
                return j;
            }
            else
            {
                var responseMessage = await response.Content.ReadAsStringAsync();
                var x = JsonConvert.DeserializeObject<ErrorHelper>(responseMessage);
                throw new Exception(x.ExceptionMessage);
            }

        }

    }
}
