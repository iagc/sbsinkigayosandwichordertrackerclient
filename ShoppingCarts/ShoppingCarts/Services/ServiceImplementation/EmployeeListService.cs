﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using MvvmHelpers;
using ShoppingCarts.Services.ServiceInterface;

namespace ShoppingCarts.Services.ServiceImplementation
{
    public class EmployeeListService : IEmployeeListService
    {
        public List<EmployeeFoodAllowanceModel> EmployeeFoodAllowanceList;
        public HttpManager HttpManager;

        public EmployeeListService()
        {
            HttpManager = new HttpManager();
        }
        public async Task<List<CrpModel>> GetCRPList(string term)
        {
            List<CrpModel> response;

            if (string.IsNullOrEmpty(term) && string.IsNullOrWhiteSpace(term))
            {
                response = await HttpManager.GetAsync<CrpModel>(Constants.SalesCRPList());
            }
            else
            {
                response = await HttpManager.GetAsync<CrpModel>($"{Constants.SalesCRPList()}/{term}");
            }
            
            return response;
        }

        public async Task<List<EmployeeFoodAllowanceModel>> GetEmployeeFoodAllowanceList(string term)
        {
            List<EmployeeFoodAllowanceModel> response;

            if (string.IsNullOrEmpty(term) && string.IsNullOrWhiteSpace(term))
            {
                response = await HttpManager.GetAsync<EmployeeFoodAllowanceModel>(Constants.EmployeeFoodAllowanceList());
            } else
            {
                response = await HttpManager.GetAsync<EmployeeFoodAllowanceModel>($"{Constants.EmployeeFoodAllowanceList()}/{term}");
            }
            
            return response;
        }
    }
}
