﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using MvvmHelpers;
using ShoppingCarts.Services.ServiceInterface;

namespace ShoppingCarts.Services.ServiceImplementation
{
    public class TransactionTypeService : ITransactionTypeService
    {
        public List<TransactionType> TransactionTypeList;
        public HttpManager HttpManager;

        public TransactionTypeService()
        {
            TransactionTypeList = new List<TransactionType>();
            HttpManager = new HttpManager();
        }

        public async Task<List<TransactionType>> GetTransactionTypes()
        {
            var response = await HttpManager.GetAsync<TransactionType>(Constants.TransactionTypesURL());
            return response;
        }
    }
}
