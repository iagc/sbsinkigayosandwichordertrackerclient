﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShoppingCarts.Services;
using ShoppingCarts.Model;
using Xamarin.Forms;
using System.Net.Http;
using Newtonsoft.Json;
using ShoppingCarts.Helpers;


namespace ShoppingCarts.Services
{
    public class RestService : IRestService
    {
        readonly HttpClient _client;

        public RestService()
        {
            _client = new HttpClient { MaxResponseContentBufferSize = 256000 };
        }

        public async Task<UserInfo> LoginInServer(string userId, string password)
        {

            var payload = new { EmployeeNumber = userId, Password = password };
            var jsonObject = JsonConvert.SerializeObject(payload);
            var d = new StringContent(jsonObject, Encoding.UTF8, "application/json");


            var response = await _client.PostAsync(Constants.LogInUrl(), d);
            
            if (!response.IsSuccessStatusCode) return null;
            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<UserInfo>(content);

            return result;
        }

    }
}