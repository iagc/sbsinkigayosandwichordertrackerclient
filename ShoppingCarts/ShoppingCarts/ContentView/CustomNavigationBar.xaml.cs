﻿using ShoppingCarts.Helpers;
using ShoppingCarts.Utilities;
using ShoppingCarts.Views;
using System;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingCarts.ContentView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CustomNavigationBar : Xamarin.Forms.ContentView
    {
        public CustomNavigationBar()
        {
            InitializeComponent();
        }

        public Label FirstNameLabel => labelText;

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            MessagingCenter.Send(this, "presentMenu");
        }

        public static readonly BindableProperty TitleProperty =
        BindableProperty.Create(
        "Title",
        typeof(string),
        typeof(CustomNavigationBar),
        "this is Title",
        propertyChanged: OnTitlePropertyChanged
        );

        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }

        private static void OnTitlePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var thisView = bindable as CustomNavigationBar;
            var title = newValue.ToString();
            thisView.lblTitle.Text = title;
        }

        private async void Tapcart_OnTapped(object sender, EventArgs e)
        {
            if (SettingsChecker.AreSettingsSet())
            {
                await Navigation.PushAsync(new CartDetailPage());
            }
            else
            {
                MessagingCenter.Send<CustomNavigationBar>(this, "CartPageCheckSettings");
            }
            
        }

    }
}