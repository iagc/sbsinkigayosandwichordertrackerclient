﻿using ShoppingCarts.Model;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ShoppingCarts.Helpers
{
    class EmployeeFoodAllowanceDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate UnavailedTemplate { get; set; }
        public DataTemplate AvailedTemplate { get; set; }
        
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (item == null)
            {
                return null;
            }

            return ((EmployeeFoodAllowanceModel)item).HasAvailed ? AvailedTemplate : UnavailedTemplate;

        }
    }
}
