﻿using Plugin.Connectivity;

namespace ShoppingCarts.Helpers
{
    public static class NetworkCheck
    {
        public static bool IsInternet()
        {
            return CrossConnectivity.Current.IsConnected;
        }
    }
}