﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Helpers.Interface
{
    public interface IAppVersion
    {
        string GetVersion();
        int GetBuild();

    }
}
