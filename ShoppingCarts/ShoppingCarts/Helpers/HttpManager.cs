﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using System;

namespace ShoppingCarts.Helpers
{
    public class HttpManager
    {
        public HttpManager()
        {
        }

        public async Task<List<T>> GetAsync<T>(string requestUrl) where T : class
        {
            var client = new System.Net.Http.HttpClient();
            var response = await client.GetAsync(requestUrl);
            if (!response.IsSuccessStatusCode)
            {
                var responseMessage = await response.Content.ReadAsStringAsync();
                throw new Exception(responseMessage);
            }
            var responseJson = await response.Content.ReadAsStringAsync();
            var jsonObject = JsonConvert.DeserializeObject<List<T>>(responseJson);
            return jsonObject;
        }

        public async Task<T> GetSingleAsync<T>(string requestUrl) where T : class
        {
            var client = new HttpClient();
            var response = await client.GetAsync(requestUrl);

            if (!response.IsSuccessStatusCode)
            {
                var responseMessage = await response.Content.ReadAsStringAsync();
                throw new Exception(responseMessage);
            }

            var responseJson = await response.Content.ReadAsStringAsync();
            var jsonObject = JsonConvert.DeserializeObject<T>(responseJson);
            return jsonObject;
        }
        public async Task<double> GetAsyncDouble(string requestUrl)
        {
            var client = new HttpClient();
            var response = await client.GetAsync(requestUrl);

            if (!response.IsSuccessStatusCode)
            {
                var responseMessage = await response.Content.ReadAsStringAsync();
                throw new Exception(responseMessage);
            }

            string result = await response.Content.ReadAsStringAsync();

            double doubleresult = Convert.ToDouble(result);
            
            return doubleresult;
        }
    }
}

