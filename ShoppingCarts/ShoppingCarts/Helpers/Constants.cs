﻿using System.ComponentModel;

namespace ShoppingCarts.Helpers
{
    public static class Constants
    {
        public static string ProductUrl() { return GetBaseUrl() + "Product"; }
        public static string LogInUrl() { return GetBaseUrl() + "Login"; }
        public static string SalesUrl() { return GetBaseUrl() + "Sales"; }
        public static string SavePosBillUrl() { return GetBaseUrl() + "PosBillApps"; }
        public static string GcUrl() { return GetBaseUrl() + "GiftCertificates"; }
        public static string PromoItemURL() { return GetBaseUrl() + "PromoApps"; }
        public static string PromoItemSelectionURL() { return GetBaseUrl() + "PromoBundle"; }
        public static string DiscountTypesURL() { return GetBaseUrl() + "DiscountTypes"; }
        public static string TransactionTypesURL() { return GetBaseUrl() + "POSTransactionTypes"; }
        public static string SalesCRPList() { return GetBaseUrl() + "Crp"; }
        public static string EmployeeFoodAllowanceList() { return GetBaseUrl() + "EmployeeFoodAllowance"; }

        public const string HostNameKey = "hostName";
        public const string TerminalNumberKey = "terminalNumber";
        public const string AutoGenerateQueueNumberKey = "autoGenerateQueueNumber";
        public const string AppScenarioKey = "appScenario";
        public const string AppConfigKey = "appConfig";
        public const string HappyHourStartTimeKey = "happyHourStartTimeKey";
        public const string HappyHourEndTimeKey = "happyHourEndTimeKey";
        public const string HappyHourPromoIdKey = "happyHourPromoIdKey";
        public const string HappyHourEnabledKey = "happyHourEnabledKey";
        public const string ForceCrpSelectionKey = "happyHourEnabledKey";

        public const int OrderPageSize = 10;        

        public static string GetBaseUrl()
        {
            if (App.Settings.AppConfig.ToLowerInvariant() == "live")
            {
                return "http://sbsapi.cfihost.com/api/";
            }
            else
            {
                return "http://sbsapitest.cfihost.com/api/";
            }
        }

        public enum OrderStatus
        {
            [Description("all")]
            All,
            [Description("served")]
            Served,
            [Description("unserved")]
            Unserved,
            [Description("void")]
            Void
        }
    }
}