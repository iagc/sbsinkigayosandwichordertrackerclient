﻿using ShoppingCarts.Model;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ShoppingCarts.Helpers
{
    class OrderDataTemplateSelector : DataTemplateSelector
    {                
        public DataTemplate ServedTemplate { get; set; }
        public DataTemplate UnservedTemplate { get; set; }
        public DataTemplate VoidTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (item == null)
            {
                return null;
            }

            return ((SalesSummary)item).Migrated ? ServedTemplate : ((SalesSummary)item).SalesIsVoid == 1 ? VoidTemplate : UnservedTemplate;

        }
    }
    
}
