﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace ShoppingCarts.Helpers
{
    public static class Settings
    {
        private static ISettings Setting => CrossSettings.Current;

        public static bool ItemStatus1
        {
            get => Setting.GetValueOrDefault("ItemStatus1", false);
            set => Setting.AddOrUpdateValue("ItemStatus1", value);
        }

        public static bool ItemStatus2
        {
            get => Setting.GetValueOrDefault("ItemStatus2", false);
            set => Setting.AddOrUpdateValue("ItemStatus2", value);
        }

        public static bool ItemStatus3
        {
            get => Setting.GetValueOrDefault("ItemStatus3", false);
            set => Setting.AddOrUpdateValue("ItemStatus3", value);
        }

        public static bool ItemStatus4
        {
            get => Setting.GetValueOrDefault("ItemStatus4", false);
            set => Setting.AddOrUpdateValue("ItemStatus4", value);
        }

        public static bool ItemStatus5
        {
            get => Setting.GetValueOrDefault("ItemStatus5", false);
            set => Setting.AddOrUpdateValue("ItemStatus5", value);
        }

        public static bool ItemStatus6
        {
            get => Setting.GetValueOrDefault("ItemStatus6", false);
            set => Setting.AddOrUpdateValue("ItemStatus6", value);
        }

        public static bool ItemStatus7
        {
            get => Setting.GetValueOrDefault("ItemStatus7", false);
            set => Setting.AddOrUpdateValue("ItemStatus7", value);
        }

        public static bool ItemStatus8
        {
            get => Setting.GetValueOrDefault("ItemStatus8", false);
            set => Setting.AddOrUpdateValue("ItemStatus8", value);
        }

        public static bool ItemStatus9
        {
            get => Setting.GetValueOrDefault("ItemStatus9", false);
            set => Setting.AddOrUpdateValue("ItemStatus9", value);
        }

        public static bool ItemStatus10
        {
            get => Setting.GetValueOrDefault("ItemStatus10", false);
            set => Setting.AddOrUpdateValue("ItemStatus10", value);
        }

        public static bool ItemStatus11
        {
            get => Setting.GetValueOrDefault("ItemStatus11", false);
            set => Setting.AddOrUpdateValue("ItemStatus11", value);
        }

        public static bool ItemStatus12
        {
            get => Setting.GetValueOrDefault("ItemStatus12", false);
            set => Setting.AddOrUpdateValue("ItemStatus12", value);
        }

        public static bool ItemStatus13
        {
            get => Setting.GetValueOrDefault("ItemStatus13", false);
            set => Setting.AddOrUpdateValue("ItemStatus13", value);
        }

        public static bool ItemStatus14
        {
            get => Setting.GetValueOrDefault("ItemStatus14", false);
            set => Setting.AddOrUpdateValue("ItemStatus14", value);
        }

        public static bool ItemStatus15
        {
            get => Setting.GetValueOrDefault("ItemStatus15", false);
            set => Setting.AddOrUpdateValue("ItemStatus15", value);
        }
    }
}