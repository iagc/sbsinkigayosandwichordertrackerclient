﻿using ShoppingCarts.Helpers.Interface;
using ShoppingCarts.Model;
using SQLite.Net;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ShoppingCarts.Helpers
{
    public class DataAccess
    {
        private readonly SQLiteConnection _dbConn;

        public DataAccess()
        {
            _dbConn = DependencyService.Get<ISQLite>().GetConnection();
            // create the table(s)
            _dbConn.CreateTable<Product>();
        }

        public List<Product> GetAllProducts()
        {
            return _dbConn.Query<Product>("Select * From [Product]");
        }

        public int SaveProduct(Product aProduct)
        {
            return _dbConn.Insert(aProduct);
        }

        public int DeleteProduct(Product aProduct)
        {
            return _dbConn.Delete(aProduct);
        }

        public int EditProduct(Product aProduct)
        {
            return _dbConn.Update(aProduct);
        }
    }
}