﻿namespace ShoppingCarts.Helpers
{
    public static class GenericMethods
    {
        public static int CartCount()
        {
            return Calculate();
        }


        public static int Calculate()
        {

            int ct = 0;
            foreach (var item in App.Cart)
            {
                ct = ct + item.Quantity;
            }
            return ct;
        }

        public static double GetTotalAmount()
        {
            double TotalAmount = 0;
            foreach (var item in App.Cart)
            {
                TotalAmount = TotalAmount + item.Subtotal;
            }
            return TotalAmount;
        }
    }
}