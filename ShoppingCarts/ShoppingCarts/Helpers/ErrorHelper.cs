﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Helpers
{
    public class ErrorHelper
    {
        public string Message { get; set; }
        public string ExceptionMessage { get; set; }
    }
}
