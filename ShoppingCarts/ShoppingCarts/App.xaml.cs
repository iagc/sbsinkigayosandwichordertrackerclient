﻿using DLToolkit.Forms.Controls;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using ShoppingCarts.Helpers;
using Xamarin.Forms;
using ShoppingCarts.Services;
using System.Collections.Generic;
using ShoppingCarts.Model;
using ShoppingCarts.Views;
using ShoppingCarts.Utilities;
using Xamarin.Essentials;
using System;

namespace ShoppingCarts
{
    public partial class App : Application
    {
        public static string User = "UserOne";
        private static DataAccess dbUtils;

        //TODO: Implement Global Styles in App.xaml

        public App()
        {
            InitializeComponent();

            FlowListView.Init();

            Cart = new List<ProductDetail>();
            Settings = new SettingsClass();            
            MainPage = new NavigationPage(new Views.LoginPage());

        }

        public static INavigationService NavigationService { get; } = new FormsNavigationService();
        public static List<ProductDetail> Cart { get; set; }
        public static SettingsClass Settings { get; set; }
        public static bool IsUserLoggedIn { get; set; }
        public static string LoggedInUserId { get; set; }

        public static DataAccess DaUtil => dbUtils ?? (dbUtils = new DataAccess());

        protected override async void OnStart()
        {
            // Handle when your app starts

            // App center analytices and crashes
            AppCenter.Start(ApiKeys.AndroidAppCenterKey + ApiKeys.iOSAppCenterKey, typeof(Analytics), typeof(Crashes));

            Settings.HostName = await SecureStorage.GetAsync(Constants.HostNameKey);
            Settings.TerminalNumber = await SecureStorage.GetAsync(Constants.TerminalNumberKey);
            Settings.AutoGenerateQueueNumber = await SecureStorage.GetAsync(Constants.AutoGenerateQueueNumberKey);
            Settings.Scenario = await SecureStorage.GetAsync(Constants.AppScenarioKey);
            Settings.AppConfig = await SecureStorage.GetAsync(Constants.AppConfigKey);

            var s = await SecureStorage.GetAsync(Constants.HappyHourStartTimeKey);
            if (long.TryParse(s, out var resultStart))
            {
                Settings.HappyHourStartTime = new TimeSpan(resultStart);
            }
            else
            {
                Settings.HappyHourStartTime = new TimeSpan(13, 0, 0);
            }

            var e = await SecureStorage.GetAsync(Constants.HappyHourEndTimeKey);
            if (long.TryParse(e, out long resultEnd))
            {
                Settings.HappyHourEndTime = new TimeSpan(resultEnd);
            }
            else
            {
                Settings.HappyHourEndTime = new TimeSpan(16, 0, 0);
            }

            var p = await SecureStorage.GetAsync(Constants.HappyHourPromoIdKey);
            if (int.TryParse(p, out int resultPromoId))
            {
                Settings.HappyHourPromoId = resultPromoId;
            }
            else
            {
                Settings.HappyHourPromoId = 7;
            }

            var m = await SecureStorage.GetAsync(Constants.HappyHourEnabledKey);
            if (bool.TryParse(m, out bool resultHappyHourEnabled))
            {
                Settings.HappyHourEnabled = resultHappyHourEnabled;
            }
            else
            {
                Settings.HappyHourEnabled = true;
            }

            var n = await SecureStorage.GetAsync(Constants.ForceCrpSelectionKey);
            if (bool.TryParse(n, out bool resultForceCrpSelection))
            {
                Settings.ForceCrpSelection = resultForceCrpSelection;
            }
            else
            {
                Settings.ForceCrpSelection = true;
            }

        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}