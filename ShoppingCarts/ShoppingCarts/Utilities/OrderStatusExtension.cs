﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using static ShoppingCarts.Helpers.Constants;

namespace ShoppingCarts.Utilities
{
    public static class OrderStatusExtensions
    {
        public static string ToDescriptionString(this OrderStatus val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
