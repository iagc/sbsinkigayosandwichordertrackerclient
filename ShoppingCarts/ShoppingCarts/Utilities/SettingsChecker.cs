﻿using ShoppingCarts.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace ShoppingCarts.Utilities
{
    public static class SettingsChecker
    {
        public static bool AreSettingsSet()
        {

            if (App.Settings.HostName == null)
                return false;

            if (App.Settings.TerminalNumber == null)
                return false;

            if (App.Settings.AutoGenerateQueueNumber == null)
                return false;

            if (App.Settings.Scenario == null)
                return false;

            if (App.Settings.AppConfig == null)
                return false;

            return App.Settings.HostName.Trim() != string.Empty
                && App.Settings.TerminalNumber.Trim() != string.Empty
                && App.Settings.AutoGenerateQueueNumber.Trim() != string.Empty
                && App.Settings.Scenario.Trim() != string.Empty
                && App.Settings.AppConfig.Trim() != string.Empty;
        }
    }
}
