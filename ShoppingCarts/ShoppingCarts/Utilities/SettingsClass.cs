﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Utilities
{
    public class SettingsClass
    {
        public string HostName { get; set; }
        public string TerminalNumber { get; set; }
        public string AutoGenerateQueueNumber { get; set; }
        public string Scenario { get; set; }
        public string AppConfig { get; set; }
        public TimeSpan HappyHourStartTime { get; set; }
        public TimeSpan HappyHourEndTime { get; set; }
        public int HappyHourPromoId { get; set; }
        public bool HappyHourEnabled { get; set; }
        public bool ForceCrpSelection { get; set; }
    }
}
