﻿using MvvmHelpers;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using ShoppingCarts.Services;
using ShoppingCarts.Services.ServiceImplementation;
using ShoppingCarts.Utilities;
using ShoppingCarts.Views;
using Xamarin.Essentials;

namespace ShoppingCarts.ViewModels
{
    public class DiscountTypesViewModel : BaseViewModel
    {
        public Command GetData { get; set; }
        public readonly IDiscountTypeService Service;

        private double discountAmount { get; set; }

        public double DiscountAmount
        {
            get { return discountAmount; }
            set
            {
                if (DiscountAmount != value)
                {
                    discountAmount = value;
                }
            }
        }

        private double totalAmount { get; set; }

        public double TotalAmount
        {
            get { return totalAmount; }
            set
            {
                if (TotalAmount != value)
                {
                    totalAmount = value;
                }
            }
        }
        private DiscountType selectedItem { get; set; }
        public DiscountType SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (SelectedItem != value)
                {
                    selectedItem = value;
                }
            }
        }

        private ObservableRangeCollection<DiscountType> _details;
        public ObservableRangeCollection<DiscountType> Details
        {
            get => _details;
            set
            {
                _details = value;
                OnPropertyChanged("Details");
            }
        }

        public DiscountTypesViewModel()
        {
            Details = new ObservableRangeCollection<DiscountType>();
            GetData = new Command(async () => await GetDataCommand());
            Service = new DiscountTypeService();
        }

        public async Task<double> SendDiscount()
        {
            if (IsBusy)
                return 0;
            try
            {
                IsBusy = true;
                if (NetworkCheck.IsInternet())
                {   
                    discountAmount = await Service.GetDiscountAmount(selectedItem.Code, totalAmount);
                    return discountAmount;
                }
                else
                {
                    MessagingCenter.Send(this, "NetworkAlert");
                    return 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is : " + ex);
                return 0;
            }
            finally
            {
                IsBusy = false;
            }
        }
        public async Task GetDataCommand()
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;
                if (NetworkCheck.IsInternet())
                {
                    var result = await Service.GetDiscountTypes();
                    //var result = posBillList.OrderByDescending(x => x.Denomination);

                    Details.Clear();
                    Details.ReplaceRange(result);
                }
                else
                {
                    MessagingCenter.Send(this, "NetworkAlert");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is : " + ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
