﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AppCenter.Analytics;
using MvvmHelpers;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceImplementation;
using ShoppingCarts.Services.ServiceInterface;
using Xamarin.Forms;

namespace ShoppingCarts.ViewModels
{
    public class OrdersPageViewModel : BaseViewModel
    {

        public ObservableRangeCollection<SalesSummary> Orders { get; } = new ObservableRangeCollection<SalesSummary>();
        public Command GetDataCommand { get; set; }
        private readonly ISalesService _Service;

        public int Position { get; set; }

        public Constants.OrderStatus Status { get; set; }
        public DateTime TransactionDate { get; set; }

        public OrdersPageViewModel()
        {
            _Service = new SalesService();
        }

        public async Task GetSalesTask()
        {
            try
            {
                if (IsBusy == false)
                {
                    IsBusy = true;
                    if (NetworkCheck.IsInternet())
                    {
                        var sales = await _Service.GetSalesTask(Position, Constants.OrderPageSize, TransactionDate, Status);
                        if (sales.Count > 0)
                        {
                            foreach (var order in from sale in sales
                                                  from order in Orders
                                                  where sale.SalesInvoiceNumber == order.SalesInvoiceNumber
                                                  select order)
                            {
                                Orders.Remove(order);
                            }

                            Orders.AddRange(sales);
                        }
                    }
                    else
                    {
                        MessagingCenter.Send(this, "NetworkAlert");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is : " + ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

    }
}
