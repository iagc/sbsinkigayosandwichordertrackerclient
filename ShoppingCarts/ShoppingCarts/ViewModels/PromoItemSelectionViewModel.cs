﻿using MvvmHelpers;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using ShoppingCarts.Services;
using ShoppingCarts.Services.ServiceImplementation;
using ShoppingCarts.Utilities;
using ShoppingCarts.Views;
using Xamarin.Essentials;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace ShoppingCarts.ViewModels
{
    public class PromoItemSelectionViewModel : BaseViewModel, INotifyPropertyChanged
    {
        public Command GetData { get; set; }        
        public Command CheckQty { get; set; }
        public PromoSummary Promo { get; set; } = new PromoSummary();

        private int promoQuantity;
        public int PromoQuantity
        {
            get => promoQuantity;
            set
            {
                if (promoQuantity != value)
                {
                    if (value == 0)
                        value = 1;

                    promoQuantity = value;
                    ProcessResult();
                }        
            }
        }

        public bool disablePromoqty;
        public bool DisablePromoQty
        {
            get => disablePromoqty;
            set
            {
                if (disablePromoqty != value)
                {
                    disablePromoqty = value;
                }
            }
        }

        public readonly IPromoItemSelectionService Service;

        public List<PromoCustomizationModel> Output = new List<PromoCustomizationModel>();
        public List<PromoItemSelectionModel> ResultFromApi = new List<PromoItemSelectionModel>();
        public ObservableCollection<Grouping<string, PromoCustomizationModel>> DetailsGrouped { get; set; } = new ObservableCollection<Grouping<string, PromoCustomizationModel>>();

        public PromoItemSelectionViewModel()
        {            
            GetData = new Command(async () => await GetDataCommand());
            Service = new PromoItemSelectionService();
            PromoQuantity = 1;
        }

        public void ProcessResult(Voucher voucher = null)
        {
            Output.Clear();
            foreach (var item in ResultFromApi)
            {
                foreach (var item2 in item.PromoItems)
                {
                    var i = new PromoCustomizationModel()
                    {
                        bundlename = item.bundlename,
                        id = item.id,
                        required = item.required,
                        totalqty = (item.totalqty * promoQuantity),
                        Barcode = item2.Barcode,
                        Category = item2.Category,
                        FullDescription = item2.FullDescription,
                        ShortDescription = item2.ShortDescription,
                        OrigPrice = item2.OrigPrice,
                        Price = item2.Price,
                        PromoDescription = item2.PromoDescription,
                        PromoID = item2.PromoID,
                        Quantity = item.required ? 0 : (1 * promoQuantity),
                        Status = item2.Status,
                        Subtotal = item2.Subtotal,
                        Voucher = voucher
                    };

                    i.PropertyChanged += I_PropertyChanged;

                    Output.Add(i);
                }
            }

            var sorted = Output.OrderBy(p => p.GetName())
           .GroupBy(p => p.GetName())
           .Select(p => new Grouping<string, PromoCustomizationModel>(p.Key, p));

            DetailsGrouped = new TrulyObservableCollection<Grouping<string, PromoCustomizationModel>>(sorted);

        }

        public async Task GetDataCommand()
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;
                if (NetworkCheck.IsInternet())
                {
                    var result = await Service.GetPromoItemsSelection(Promo.PromoID);
                    ResultFromApi = result;
                    var v = (from p in Promo.PromoItems where p.Voucher != null select p).FirstOrDefault();
                    ProcessResult(v.Voucher);
                }
                else
                {
                    MessagingCenter.Send(this, "NetworkAlert");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is : " + ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void I_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.ToLower() == "quantity")
            {
                var item = (PromoCustomizationModel)sender;
                if (item.Quantity == 0)
                    return;

                var result = Output
                    .Where(b => b.bundlename == item.bundlename)
                    .GroupBy(x => new { x.bundlename, x.totalqty })
                    .Select(g => new
                    {
                        BundleName = g.Key.bundlename,
                        g.Key.totalqty,
                        Total = g.Sum(x => x.Quantity)
                    }).SingleOrDefault();
                
                if (result.Total > result.totalqty)
                {
                    foreach (var itm in DetailsGrouped.Where(x => x.Key == item.GetName()))
                    {
                        foreach (var itm2 in itm)
                        {
                            if (itm2 != item)
                            {
                                itm2.Quantity = 0;
                            } else
                            {
                                item.Quantity = item.totalqty;
                            }
                        }
                    }
                }

            }
        }
    }
}
