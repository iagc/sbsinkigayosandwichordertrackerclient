﻿using MvvmHelpers;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using ShoppingCarts.Services;
using ShoppingCarts.Services.ServiceImplementation;
using ShoppingCarts.Utilities;
using ShoppingCarts.Views;
using Xamarin.Essentials;

namespace ShoppingCarts.ViewModels
{
    public class EmployeeFoodAllowanceViewModel : BaseViewModel
    {
        public Command GetData { get; set; }
        public readonly IEmployeeListService Service;
        private EmployeeFoodAllowanceModel selectedItem { get; set; }
        public EmployeeFoodAllowanceModel SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (SelectedItem != value)
                {
                    selectedItem = value;
                }
            }
        }

        private ObservableRangeCollection<EmployeeFoodAllowanceModel> _details;
        public ObservableRangeCollection<EmployeeFoodAllowanceModel> Details
        {
            get => _details;
            set
            {
                _details = value;
                OnPropertyChanged("Details");
            }
        }

        public EmployeeFoodAllowanceViewModel()
        {
            Details = new ObservableRangeCollection<EmployeeFoodAllowanceModel>();
            GetData = new Command<string>(async (x) => await GetDataCommand(x));
            Service = new EmployeeListService();
        }

        public async Task GetDataCommand(string term)
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;
                if (NetworkCheck.IsInternet())
                {
                    var result = await Service.GetEmployeeFoodAllowanceList(term);
                    Details.Clear();
                    Details.ReplaceRange(result);
                }
                else
                {
                    MessagingCenter.Send(this, "NetworkAlert");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is : " + ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

    }
}
