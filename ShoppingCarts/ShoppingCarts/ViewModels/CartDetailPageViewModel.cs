﻿using Microsoft.AppCenter.Analytics;
using MvvmHelpers;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceImplementation;
using ShoppingCarts.Services.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace ShoppingCarts.ViewModels
{
    public class CartDetailPageViewModel : BaseViewModel
    {
        public ObservableRangeCollection<ProductDetail> Products { get; } = new ObservableRangeCollection<ProductDetail>();


        public bool ItemsInCart { get; set; }
        public bool NoItemsInCart { get; set; }
        public string PromoID { get; set; }
        public Command GetData { get; set; }
        public Command OnItemButtonClickedCommand { get; set; }
        public Command RemoveAllButton { get; set; }

        public int QueueNumber { get; set; }
        public bool IsPreOrder { get; set; }

        public readonly IProductDetailService Service;
        public readonly IPromoItemService PromoService;
        public readonly ISalesService SalesService;

        public CartDetailPageViewModel()
        {
            GetData = new Command(GetDataCommand);
            Service = DependencyService.Get<IProductDetailService>();
            SalesService = new SalesService();
            OnItemButtonClickedCommand = new Command(ExecuteButtonClick);
            RemoveAllButton = new Command(RemoveAllItems);
        }

        public async Task<OrderSuccess> PlaceOrder()
        {
            if (IsBusy)
                return new OrderSuccess(false, null);

            try
            {
                var details = new List<SaleDetailModel>();
                var payments = new List<SalePaymentModel>();

                var hostName = App.Settings.HostName;
                var terminalNumber = App.Settings.TerminalNumber;


                foreach (var detail in App.Cart)
                {
                    details.Add(new SaleDetailModel
                    {
                        Barcode = detail.Barcode,
                        Description = detail.FullDescription,
                        Discount = 0,
                        OrigPrice = detail.OrigPrice,
                        Price = detail.Price,
                        Quantity = detail.Quantity,
                        DiscountRef = detail.PromoID,
                        Voucher = detail.Voucher
                    });
                }

                var net = details.Sum(s => (s.Quantity * s.Price) - s.Discount);

                var sale = new SaleModel
                {
                    CashierEmployeeNumber = App.LoggedInUserId,
                    CompanyCode = "SBS",
                    ChangeAmount = 0,
                    CustomerAddress = string.Empty,
                    CustomerBusinessStyle = string.Empty,
                    CustomerContactNumber = string.Empty,
                    CustomerName = string.Empty,
                    CustomerTin = string.Empty,
                    HostName = hostName,
                    TerminalNumber = terminalNumber,
                    TicketDiscount = 0,
                    SaleDetails = details,
                    SalePayments = payments,
                    AutoGenerateQueueNumber = false,
                    QueueNumber = this.QueueNumber,
                    IsDirectToCashier = false,
                    IsPreOrder = true,
                };

                var p = await SalesService.PostSalesTask(sale);
                
                App.Cart.Clear();
                return new OrderSuccess(true, p);

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                IsBusy = false;
            }

        }

        private void RemoveAllItems()
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;

                App.Cart.Clear();
                Products.Clear();

                ItemsInCart = false;
                NoItemsInCart = true;

                Analytics.TrackEvent("Remove all items from cart clicked");
            }
            catch (Exception e)
            {
                Console.Write("Exception is " + e);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void ExecuteButtonClick(object e)
        {
            if (IsBusy)
                return;

            try
            {
                IsBusy = true;
                var selectedItem = (ProductDetail)e;
                if (NetworkCheck.IsInternet())
                {
                    var productsList = App.Cart;
                    var found = false;
                    var removeitem = false;
                    if (App.Cart.Count >= 1)
                    {
                        foreach (var item in App.Cart)
                        {
                            if (item.Barcode == selectedItem.Barcode && item.PromoID == selectedItem.PromoID && item.Price == selectedItem.Price)
                            {
                                found = true;
                                item.Quantity--;

                                if (selectedItem.PromoID != "")
                                {
                                    App.Cart.RemoveAll(x => x.PromoID == selectedItem.PromoID);
                                    break;
                                }
                            }

                            if (item.Quantity == 0)
                            {
                                removeitem = true;
                            }

                        }
                    }

                    if (!found || removeitem == true)
                    {
                        App.Cart.Remove(selectedItem);
                    }

                    productsList = App.Cart;
                    Products.Clear();
                    SetItems(productsList);
                    Products.ReplaceRange(productsList);

                }
                else
                {
                    MessagingCenter.Send(this, "NetworkAlert");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is " + ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        private void GetDataCommand()
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;
                if (NetworkCheck.IsInternet())
                {
                    var productsList = App.Cart;

                    Products.Clear();
                    SetItems(productsList);
                    Products.ReplaceRange(productsList);
                }
                else
                {
                    MessagingCenter.Send(this, "NetworkAlert");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception is : " + e);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void SetItems(List<ProductDetail> productList)
        {
            if (productList.Count > 0)
            {
                ItemsInCart = true;
                NoItemsInCart = false;
            }
            else
            {
                ItemsInCart = false;
                NoItemsInCart = true;
            }
        }
    }
}