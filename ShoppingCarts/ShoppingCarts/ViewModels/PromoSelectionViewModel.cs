﻿using MvvmHelpers;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using ShoppingCarts.Services;
using ShoppingCarts.Services.ServiceImplementation;
using ShoppingCarts.Utilities;
using ShoppingCarts.Views;
using Xamarin.Essentials;

namespace ShoppingCarts.ViewModels
{
    public class PromoSelectionViewModel : BaseViewModel
    {
        public Command GetData { get; set; }
        public readonly IPromoSelectionService Service;

        private PromoSummary selectedItem { get; set; }
        public PromoSummary SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (SelectedItem != value)
                {
                    selectedItem = value;
                }
            }
        }

        private ObservableRangeCollection<PromoSummary> _details;
        public ObservableRangeCollection<PromoSummary> Details
        {
            get => _details;
            set
            {
                _details = value;
                OnPropertyChanged("Details");
            }
        }

        public PromoSelectionViewModel()
        {
            Details = new ObservableRangeCollection<PromoSummary>();
            GetData = new Command(async () => await GetDataCommand());
            Service = new PromoSelectionService();
        }

        public async Task GetDataCommand()
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;
                if (NetworkCheck.IsInternet())
                {
                    var result = await Service.GetPromoList();
                    //var result = posBillList.OrderByDescending(x => x.Denomination);

                    Details.Clear();
                    Details.ReplaceRange(result);
                }
                else
                {
                    MessagingCenter.Send(this, "NetworkAlert");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is : " + ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    } 
}
