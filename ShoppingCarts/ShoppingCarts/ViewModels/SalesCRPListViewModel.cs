﻿using MvvmHelpers;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using ShoppingCarts.Services;
using ShoppingCarts.Services.ServiceImplementation;
using ShoppingCarts.Utilities;
using ShoppingCarts.Views;
using Xamarin.Essentials;

namespace ShoppingCarts.ViewModels
{
    public class SalesCRPListViewModel : BaseViewModel
    {
        public Command GetData { get; set; }
        public readonly IEmployeeListService Service;
        private CrpModel selectedItem { get; set; }
        public CrpModel SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (SelectedItem != value)
                {
                    selectedItem = value;
                }
            }
        }

        private ObservableRangeCollection<CrpModel> _details;
        public ObservableRangeCollection<CrpModel> Details
        {
            get => _details;
            set
            {
                _details = value;
                OnPropertyChanged("Details");
            }
        }   

        public SalesCRPListViewModel()
        {
            Details = new ObservableRangeCollection<CrpModel>();
            GetData = new Command<string>(async (x) => await GetDataCommand(x));
            Service = new EmployeeListService();
        }

        public async Task GetDataCommand(string term)
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;
                if (NetworkCheck.IsInternet())
                {
                    var result = await Service.GetCRPList(term);
                    Details.Clear();
                    Details.ReplaceRange(result);
                }
                else
                {
                    MessagingCenter.Send(this, "NetworkAlert");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is : " + ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
