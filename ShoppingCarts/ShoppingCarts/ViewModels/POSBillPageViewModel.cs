﻿using MvvmHelpers;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using ShoppingCarts.Services;
using ShoppingCarts.Services.ServiceImplementation;
using ShoppingCarts.Utilities;
using ShoppingCarts.Views;
using Xamarin.Essentials;

namespace ShoppingCarts.ViewModels
{
    public class PosBillPageViewModel : BaseViewModel
    {
        public Command GetData { get; set; }
        public readonly IPosBillService Service;

        private ObservableRangeCollection<PosBillDetails> _details;
        public ObservableRangeCollection<PosBillDetails> Details
        {
            get => _details;
            set
            {
                _details = value;
                OnPropertyChanged("Details");
            }
        }

        private string _storeCode;
        private string _hostName;
        private string _terminalNumber;

        public PosBillPageViewModel()
        {
            Details = new ObservableRangeCollection<PosBillDetails>();
            GetData = new Command(async () => await GetDataCommand());
            Service = new PosBillService();
        }

        public async Task GetDataCommand()
        {
            if (IsBusy)
                return;
            try
            {
                _storeCode = "HO";
                _hostName = App.Settings.HostName;
                _terminalNumber = App.Settings.TerminalNumber;

                IsBusy = true;
                if (NetworkCheck.IsInternet())
                {
                    var posBillList = await Service.GetPosBillDetails(_storeCode, _hostName, _terminalNumber);
                    var result = posBillList.OrderByDescending(x => x.Denomination);

                    Details.Clear();
                    Details.ReplaceRange(result);
                }
                else
                {
                    MessagingCenter.Send(this, "NetworkAlert");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is : " + ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task<bool> SavePosBill()
        {
            if (IsBusy)
                return false;
            try
            {
                var details = new List<PosBillDetails>();

                foreach (var detail in Details)
                {
                    details.Add(new PosBillDetails(detail.Denomination, detail.Qty));
                }

                var posBillSummary = new PosBillSummary(App.LoggedInUserId, _storeCode, _hostName, _terminalNumber, details);
                var p = await Service.PostPosBillTask(Constants.SavePosBillUrl(), posBillSummary);

                return p;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is " + ex);
            }
            finally
            {
                IsBusy = false;
            }

            return false;
        }
    }
}
