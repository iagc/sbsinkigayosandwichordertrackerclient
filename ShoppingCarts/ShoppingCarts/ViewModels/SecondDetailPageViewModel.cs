﻿using MvvmHelpers;
using ShoppingCarts.Model;

namespace ShoppingCarts.ViewModels
{
    public class SecondDetailPageViewModel : BaseViewModel
    {
        //public string imageSource;

        //public string ImageSource
        //{
        //    get { return imageSource; }
        //    set { SetProperty(ref imageSource, value); }
        //}

        public string barcode;

        public string Barcode
        {
            get { return barcode; }
            set { SetProperty(ref barcode, value); }
        }

        public string category;
        public string Category
        {
            get { return category; }
            set { SetProperty(ref category, value); }
        }

        public string shortdescription;

        public string ShortDescription
        {
            get { return shortdescription; }
            set { SetProperty(ref shortdescription, value); }
        }

        public string fulldescription;

        public string FullDescription
        {
            get { return fulldescription; }
            set { SetProperty(ref fulldescription, value); }
        }

        public double price;

        public double Price
        {
            get { return price; }
            set { SetProperty(ref price, value); }
        }
        public SecondDetailPageViewModel(ProductDetail productDetail)
        {
            //ImageSource = productDetail.ImageUrl;
            Barcode = productDetail.Barcode;
            Category = productDetail.Category;
            ShortDescription = productDetail.ShortDescription;
            FullDescription = productDetail.FullDescription;
            Price = productDetail.Price;
        }
    }
}