﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Windows.Input;
using ShoppingCarts.Services;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace ShoppingCarts.ViewModels
{
    public class LoginPageViewModel : INotifyPropertyChanged
    {
        private string _username;
        private string _password;
        private bool _areCredentialsInvalid;
        private bool _isBusy;

        public LoginPageViewModel()
        {

            AuthenticateCommand = new Command(async () =>
            {
                AreCredentialsInvalid = await UserAuthenticated(Username, Password, (result) => {

                    IsBusy = false;
                    if (result) return;
                    App.IsUserLoggedIn = true;
                    App.LoggedInUserId = Username;
                    Application.Current.MainPage = new Views.MainPage();
                });

            });

            AreCredentialsInvalid = false;

        }
        private async Task<bool> UserAuthenticated(string username, string password, Action<bool> callback)
        {
            bool result;

            if (string.IsNullOrEmpty(username)
                || string.IsNullOrEmpty(password))
            {
                return false;
            }

            IsBusy = true;

            try
            {
                var rest = new RestService();
                var UserResult = await rest.LoginInServer(username, password);
                result = UserResult.employeenumber.Trim() != "";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                result = false;
            }

            callback(!result);
            
            return !result;

        }

        public string Username
        {
            get => _username;
            set
            {
                if (value == _username) return;
                _username = value;
                OnPropertyChanged(nameof(Username));
            }
        }

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                if (value == _isBusy) return;
                _isBusy = value;
                OnPropertyChanged(nameof(IsBusy));
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                if (value == _password) return;
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        public ICommand AuthenticateCommand { get; set; }

        public bool AreCredentialsInvalid
        {
            get => _areCredentialsInvalid;
            set
            {
                if (value == _areCredentialsInvalid) return;
                _areCredentialsInvalid = value;
                OnPropertyChanged(nameof(AreCredentialsInvalid));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
