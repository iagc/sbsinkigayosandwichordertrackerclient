﻿using MvvmHelpers;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using ShoppingCarts.Services;
using ShoppingCarts.Services.ServiceImplementation;
using ShoppingCarts.Utilities;
using ShoppingCarts.Views;
using Xamarin.Essentials;

namespace ShoppingCarts.ViewModels
{
    public class CheckOutPageViewModel : BaseViewModel
    {
        public Command GetData { get; set; }
        public readonly IProductDetailService Service;
        public readonly ISalesService SalesService;
        public readonly IGcService GcService;
        public string GcNumber { get; set; }
        public List<ProductDetail> CheckoutCart { get; set; } = new List<ProductDetail>();

        private CheckOutModel checkOut;
        public CheckOutModel CheckOut
        {
            get => checkOut;
            set
            {
                if (CheckOut != value)
                {
                    checkOut = value;
                }
            }
        }

        public double ChangeAmount { get; set; }

        private double _cashAmount;
        public double CashAmount
        {
            get => _cashAmount;
            set
            {
                _cashAmount = value;
                AmountTendered = _gcAmount + _cashAmount;
                OnPropertyChanged("CashAmount");
            }
        }

        private double _gcAmount;

        public double GcAmount
        {
            get => _gcAmount;
            set
            {
                _gcAmount = value;
                AmountTendered = _gcAmount + _cashAmount;
                OnPropertyChanged("GcAmount");
            }
        }

        private double _amountTendered;

        public double AmountTendered
        {
            get => _amountTendered;
            set
            {
                _amountTendered = value;
                OnPropertyChanged("AmountTendered");
            }
        }

        private DiscountType discountType;
        public DiscountType DiscountType
        {
            get => discountType;
            set
            {
                if (DiscountType != value)
                {
                    discountType = value;
                    GetDataCommand();
                }
            }
        }

        private TransactionType transactionType;
        public TransactionType TransactionType
        {
            get => transactionType;
            set
            {
                if (TransactionType != value)
                {
                    transactionType = value;
                }
            }
        }

        private CrpModel salesCRP;
        public CrpModel SalesCRP
        {
            get => salesCRP;
            set
            {
                if (SalesCRP != value)
                {
                    salesCRP = value;
                }
            }
        }

        public string CustomerAddress { get; set; }
        public string CustomerName { get; set; }
        public string CustomerTin { get; set; }
        public string CustomerBusinessStyle { get; set; }
        public int QueueNumber { get; set; }
        public bool AutoGenerateQueueNumber { get; set; }
        public bool IsPreOrder { get; set; }
        public bool IsDirectToCashier { get; set; }
        public bool IsFromOrderDetail { get; set; }
        public string InvoiceNumber { get; set; }
        
        public CheckOutPageViewModel()
        {
            GetData = new Command(GetDataCommand);
            Service = DependencyService.Get<IProductDetailService>();
            SalesService = new SalesService();
            GcService = new GcService();
        }

        public async Task<GiftCertificate> GetGc(string gcNumber)
        {
            var result = await GcService.GetGiftCertificateTask(gcNumber);
            return result;
        }

        public async Task<OrderSuccess> PlaceOrder()
        {
            if (IsBusy)
                return new OrderSuccess(false, null);

            try
            {
                string SalesCRPNumber = "";
                var details = new List<SaleDetailModel>();
                var payments = new List<SalePaymentModel>();

                var hostName = App.Settings.HostName;
                var terminalNumber = App.Settings.TerminalNumber;

                IsBusy = true;

                foreach (var detail in this.CheckoutCart)
                {
                    details.Add(new SaleDetailModel
                    {
                        Barcode = detail.Barcode,
                        Description = detail.FullDescription,
                        Discount = 0,
                        OrigPrice = detail.OrigPrice,
                        Price = detail.Price,
                        Quantity = detail.Quantity,
                        DiscountRef = detail.PromoID,
                        Voucher = detail.Voucher
                    });
                }

                var net = details.Sum(s => (s.Quantity * s.Price) - s.Discount);

                if (CashAmount > 0)
                {
                    payments.Add(new SalePaymentModel
                    {
                        Amount = CashAmount,
                        GcNumber = "",
                        Type = PaymentType.Cash
                    });
                }

                if (GcAmount > 0)
                {
                    payments.Add(new SalePaymentModel
                    {
                        Amount = GcAmount,
                        GcNumber = this.GcNumber,
                        Type = PaymentType.Gc
                    });
                }

                if (CustomerName == null)
                {
                    CustomerName = string.Empty;
                }

                if (CustomerAddress == null)
                {
                    CustomerAddress = string.Empty;
                }

                if (CustomerBusinessStyle == null)
                {
                    CustomerBusinessStyle = string.Empty;
                }

                if (CustomerTin == null)
                {
                    CustomerTin = string.Empty;
                }

                if (this.SalesCRP != null)
                {
                    SalesCRPNumber = this.SalesCRP.EmployeeNumber;
                }


                var sale = new SaleModel
                {
                    CashierEmployeeNumber = App.LoggedInUserId,
                    CompanyCode = "SBS",
                    ChangeAmount = ChangeAmount,
                    CustomerAddress = this.CustomerAddress.Trim(),
                    CustomerBusinessStyle = this.CustomerBusinessStyle.Trim(),
                    CustomerContactNumber = "",
                    CustomerName = this.CustomerName.Trim(),
                    CustomerTin = this.CustomerTin.Trim(),
                    HostName = hostName,
                    TerminalNumber = terminalNumber,
                    TicketDiscount = 0,
                    SaleDetails = details,
                    SalePayments = payments,
                    Discounttype = null,
                    AutoGenerateQueueNumber = this.AutoGenerateQueueNumber,
                    QueueNumber = this.QueueNumber,
                    IsDirectToCashier = this.IsDirectToCashier,
                    IsFromOrderDetail = this.IsFromOrderDetail,
                    TransactionType = this.TransactionType,
                    SalesCRPNumber = SalesCRPNumber
                };

                if (DiscountType != null)
                {
                    sale.TicketDiscount = discountType.DiscountAmount;
                }

                if (discountType != null)
                {
                    sale.Discounttype = discountType;
                }

                if (!this.IsFromOrderDetail)
                {
                    var p = await SalesService.PostSalesTask(sale);
                    await GcService.RedeemGiftCertificateTask(GcNumber);
                    App.Cart.Clear();
                    return new OrderSuccess(true, p);
                }
                else
                {
                    var p = await SalesService.PutSalesPaymentTask(InvoiceNumber, sale);
                    await GcService.RedeemGiftCertificateTask(GcNumber);
                    App.Cart.Clear();
                    return new OrderSuccess(true, p);
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                IsBusy = false;
            }
            
        }

        private void GetDataCommand()
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;
                if (NetworkCheck.IsInternet())
                {
                    CheckOut = new CheckOutModel();
                    var productsList = this.CheckoutCart;
                    if (productsList.Count >= 1)
                    {
                        foreach (var item in productsList)
                        {
                            item.Subtotal = item.Quantity * item.Price;
                        }
                    }
                    CheckOut.Products.Clear();
                    CheckOut.Products.ReplaceRange(productsList);
                    CheckOut.SubTotalAmount = productsList.Sum(s => s.Subtotal);
                    CheckOut.TotalAmount = productsList.Sum(s => s.Subtotal);
                    checkOut.TotalAmount -= discountType.DiscountAmount;
                }
                else
                {
                    MessagingCenter.Send(this, "NetworkAlert");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is : " + ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}