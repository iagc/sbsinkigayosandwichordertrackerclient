﻿using System.Threading.Tasks;
using MvvmHelpers;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceImplementation;
using Xamarin.Forms;

namespace ShoppingCarts.ViewModels
{
    public class OrderPageDetailViewModel : BaseViewModel
    {
        public SalesSummary Order { get; set; }
        public SalesService SalesService { get; set; } = new SalesService();

        public OrderPageDetailViewModel(SalesSummary order)
        {
            Order = order;
        }

        public OrderPageDetailViewModel()
        {
            
        }

        public async Task<bool> ServeOrder(string salesInvoiceNumber)
        {
            var result = await SalesService.PutSalesTask(salesInvoiceNumber, 0);
            return result;
        }

        public async Task<bool> VoidOrder(string salesInvoiceNumber)
        {
            var result = await SalesService.PutSalesTask(salesInvoiceNumber, 1);
            return result;
        }

    }
}