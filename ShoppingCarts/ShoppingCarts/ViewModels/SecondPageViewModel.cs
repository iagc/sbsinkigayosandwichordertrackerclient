﻿using MvvmHelpers;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using ShoppingCarts.Utilities;

namespace ShoppingCarts.ViewModels
{
    public class SecondPageViewModel : BaseViewModel
    {
        public Command GetData { get; set; }
        public string PromoID { get; set; }
        public string PromoDescription { get; set; }
        public readonly IProductDetailService Service;
        public readonly IPromoItemService PromoService;
        public ObservableRangeCollection<ProductDetail> Products { get; } = new ObservableRangeCollection<ProductDetail>();
        public ObservableRangeCollection<Grouping<string, ProductDetail>> ProductsGrouped { get; set; } = new ObservableRangeCollection<Grouping<string, ProductDetail>>();
        public string CartCounter { get; set; }
        public string ButtonText { get; set; }
        public Command OnItemButtonClickedCommand { get; set; }

        public SecondPageViewModel()
        {
            Products = new ObservableRangeCollection<ProductDetail>();
            GetData = new Command(async () => await GetDataCommand());
            Service = DependencyService.Get<IProductDetailService>();
            PromoService = DependencyService.Get<IPromoItemService>();
            OnItemButtonClickedCommand = new Command(ExecuteButtonClick);
        }   

        public bool ScanPromo(PromoSummary summary, List<PromoCustomizationModel> PromoItemList, int promoqty)
        {
            if (NetworkCheck.IsInternet())
            {
                if (PromoItemList.Count >= 1)
                {                    
                    foreach (var Promoitem in PromoItemList)
                    {   
                        var found = false;
                        Promoitem.PromoID = summary.PromoID;
                        Promoitem.PromoDescription = summary.PromoDescription;
                        if (App.Cart.Count >= 1)
                        {
                                
                            foreach (var CartItem in App.Cart)
                            {

                                if (CartItem.Barcode == Promoitem.Barcode && CartItem.PromoID == Promoitem.PromoID && CartItem.Price == Promoitem.Price &&
                                    CartItem.Voucher.VoucherCode == Promoitem.Voucher.VoucherCode)
                                {
                                    if ((CartItem.Voucher.VoucherCode == Promoitem.Voucher.VoucherCode) &&
                                        Promoitem.Voucher.VoucherCode != "" &&
                                        CartItem.Voucher.VoucherCode != "")
                                    {
                                        return false;
                                    }

                                    found = true;
                                    CartItem.Quantity += (Promoitem.Quantity * promoqty);
                                }
                            }
                        }
                        if (!found)
                        {
                            Promoitem.Quantity = (promoqty * Promoitem.Quantity);

                            if (Promoitem.Quantity > 0)
                            {
                                App.Cart.Add(new ProductDetail()
                                {
                                    Barcode = Promoitem.Barcode,
                                    Category = Promoitem.Category,
                                    FullDescription = Promoitem.FullDescription,
                                    ShortDescription = Promoitem.ShortDescription,
                                    OrigPrice = Promoitem.OrigPrice,
                                    Price = Promoitem.Price,
                                    PromoDescription = Promoitem.PromoDescription,
                                    PromoID = Promoitem.PromoID,
                                    Quantity = Promoitem.Quantity,
                                    Status = Promoitem.Status,
                                    Subtotal = Promoitem.Subtotal,
                                    Voucher = Promoitem.Voucher
                                });
                            }
                        }
                    }
                }
                CartCounter = GenericMethods.CartCount().ToString();
                return true;
            }
            else
            {
                MessagingCenter.Send(this, "NetworkAlert");
            }

            return false;

        }

        public void ExecuteButtonClick(object e)
        {
            if (e == null) throw new ArgumentNullException(nameof(e));

            if (IsBusy)
                return;

            try
            {
                IsBusy = true;
                var selectedItem = (ProductDetail)e;
                var productsList = new List<ProductDetail>(Products);

                Products.Clear();
                selectedItem.Status = 1;

                var found = false;

                if (App.Cart.Count >= 1)
                {
                    foreach (var item in App.Cart)
                    {
                        if (item.Barcode == selectedItem.Barcode && item.PromoID == selectedItem.PromoID && item.Price == selectedItem.Price)
                        {
                            found = true;
                            item.Quantity += 1;
                        }
                    }
                }

                if (!found)
                {
                    var nowTimeSpan = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    if (nowTimeSpan >= App.Settings.HappyHourStartTime
                        && nowTimeSpan <= App.Settings.HappyHourEndTime
                        && (selectedItem.Barcode == "SBS00000015" || selectedItem.Barcode == "SBS00000145")
                        && selectedItem.PromoID.Trim() == ""
                        && App.Settings.HappyHourEnabled)
                    {
                        selectedItem.Price = 99;
                        selectedItem.PromoID = App.Settings.HappyHourPromoId.ToString();
                        selectedItem.PromoDescription = "Happy Hour";
                    }

                    App.Cart.Add(selectedItem);
                }

                CartCounter = GenericMethods.CartCount().ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is " + ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task GetDataCommand()
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;
                if (NetworkCheck.IsInternet())
                {
                    var productsList = await Service.GetProducts();

                    Products.Clear();
                    Products.ReplaceRange(productsList);

                    var sorted = Products.OrderBy(p => p.Category)
                   .GroupBy(p => p.CategorySort)
                   .Select(p => new Grouping<string, ProductDetail>(p.Key, p));

                    ProductsGrouped = new ObservableRangeCollection<Grouping<string, ProductDetail>>(sorted);
                    CartCounter = GenericMethods.CartCount().ToString();

                }
                else
                {
                    MessagingCenter.Send(this, "NetworkAlert");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is : " + ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}