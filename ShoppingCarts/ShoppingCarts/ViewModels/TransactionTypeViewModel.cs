﻿using MvvmHelpers;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.Services.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using ShoppingCarts.Services;
using ShoppingCarts.Services.ServiceImplementation;
using ShoppingCarts.Utilities;
using ShoppingCarts.Views;
using Xamarin.Essentials;

namespace ShoppingCarts.ViewModels
{
    public class TransactionTypeViewModel : BaseViewModel
    {
        public Command GetData { get; set; }
        public readonly ITransactionTypeService Service;
        private TransactionType selectedItem { get; set; }
        public TransactionType SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (SelectedItem != value)
                {
                    selectedItem = value;
                }
            }
        }

        private ObservableRangeCollection<TransactionType> _details;
        public ObservableRangeCollection<TransactionType> Details
        {
            get => _details;
            set
            {
                _details = value;
                OnPropertyChanged("Details");
            }
        }

        public TransactionTypeViewModel()
        {
            Details = new ObservableRangeCollection<TransactionType>();
            GetData = new Command(async () => await GetDataCommand());
            Service = new TransactionTypeService();
        }

        public async Task GetDataCommand()
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;
                if (NetworkCheck.IsInternet())
                {
                    var result = await Service.GetTransactionTypes();
                    //var result = posBillList.OrderByDescending(x => x.Denomination);

                    Details.Clear();
                    Details.ReplaceRange(result);
                }
                else
                {
                    MessagingCenter.Send(this, "NetworkAlert");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception is : " + ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
