﻿using System;
using Microsoft.AppCenter.Analytics;
using ShoppingCarts.Model;
using ShoppingCarts.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShoppingCarts.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DiscountTypesPage : ContentPage
    {
        private double TotalAmount;

        public DiscountTypesViewModel ViewModel;
        public DiscountTypesPage(double totalAmount)
        {
            InitializeComponent();

            BindingContext = ViewModel = new DiscountTypesViewModel();
            TotalAmount = totalAmount;
            
            ItemsListView.ItemTapped += BtnAdd_Clicked;
        }

        private async void BtnAdd_Clicked(object sender, EventArgs e)
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;
             
                if (ViewModel.SelectedItem == null)
                {
                    await DisplayAlert("Error", "Please select a discount to apply", "Ok");
                    return;
                }
                
                var discountAmount = await Task.Run(() => ViewModel.SendDiscount());
                ViewModel.SelectedItem.DiscountAmount = discountAmount;

                if (discountAmount > 0)
                {
                    MessagingCenter.Send<DiscountTypesPage, DiscountType>(this, "DiscountPicked", ViewModel.SelectedItem);
                }
                else
                {
                    await DisplayAlert("Error", "There is no discount", "Ok");
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "Error Adding Discount: " + ex.Message, "Ok");
                return;
            }
            finally
            {                
                IsBusy = false;
            }

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.TotalAmount = TotalAmount;
            ViewModel.GetData.Execute(null);
        }
    }
}