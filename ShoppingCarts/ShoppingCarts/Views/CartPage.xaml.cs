﻿using Microsoft.AppCenter.Analytics;
using ShoppingCarts.Model;
using ShoppingCarts.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CartPage : ContentPage
    {
        public CartPageViewModel ViewModel;

        private bool _gotoCartDetailPage = false;

        public CartPage()
        {
            InitializeComponent();

            BindingContext = ViewModel = new CartPageViewModel();

            NavigationBarView.FirstNameLabel.SetBinding(Label.TextProperty, "CartCounter");

            _gotoCartDetailPage = false;
        }

        public CartPage(bool cartImage)
        {
            InitializeComponent();

            BindingContext = ViewModel = new CartPageViewModel();
            NavigationBarView.FirstNameLabel.SetBinding(Label.TextProperty, "CartCounter");
            _gotoCartDetailPage = true;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await Task.Run(() =>
            {
                ViewModel.GetData.Execute(null);
            });

            if (!_gotoCartDetailPage) return;
            _gotoCartDetailPage = false;
            await Navigation.PushAsync(new CartDetailPage());
        }

        private async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(((ListView)sender).SelectedItem is Item shoppingItem))
                return;

            await Navigation.PushAsync(new ItemDetailPage(shoppingItem));

            ((ListView)sender).SelectedItem = null;
        }
    }
}