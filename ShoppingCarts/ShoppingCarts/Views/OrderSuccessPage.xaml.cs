﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderSuccessPage : ContentPage
    {
        public string SalesInvoiceNumber { get; set; }
        public string QueueNumber { get; set; }
        public double ChangeAmount { get; set; }
        public bool DisplayQueueNumber { get; set; }
        public bool DisplayChangeAmount { get; set; }
        public bool IsFromOrderDetail { get; set; }

        public OrderSuccessPage()
        {
            InitializeComponent();
            BtnClose.Clicked += BtnClose_Clicked;
        }

        private void BtnClose_Clicked(object sender, EventArgs e)
        {
            Navigation.PopToRootAsync();
            if (IsFromOrderDetail)
            {
                MessagingCenter.Send<OrderSuccessPage>(this, "UpdateOrderStatusFromOrderDetail");
            }
            
        }

        public OrderSuccessPage(string salesInvoiceNumber, string queueNumber, double changeAmount, bool displayQueueNumber, bool displayChangeAmount, bool isFromOrderDetail)
        {
            InitializeComponent();
            BtnClose.Clicked += BtnClose_Clicked;
            SalesInvoiceNumber = salesInvoiceNumber;
            QueueNumber = queueNumber;
            ChangeAmount = changeAmount;
            DisplayQueueNumber = displayQueueNumber;
            DisplayChangeAmount = displayChangeAmount;
            IsFromOrderDetail = isFromOrderDetail;
            BindingContext = this;
            
            if (displayQueueNumber)
            {
                LblChangeAmount.FontSize = 84;
            }

        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

    }
}