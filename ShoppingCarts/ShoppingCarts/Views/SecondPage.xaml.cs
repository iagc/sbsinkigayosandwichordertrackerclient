﻿using Microsoft.AppCenter.Analytics;
using ShoppingCarts.ContentView;
using ShoppingCarts.Model;
using ShoppingCarts.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SecondPage : ContentPage
    {
        public SecondPageViewModel ViewModel;

        public SecondPage()
        {
            InitializeComponent();

            BindingContext = ViewModel = new SecondPageViewModel();
            App.Cart.Clear();
            NavigationBarView.FirstNameLabel.SetBinding(Label.TextProperty, "CartCounter");
            NavigationBarView.Title = $"Product List ({App.Settings.AppConfig})";

            MessagingCenter.Subscribe<CustomNavigationBar>(this, "CartPageCheckSettings", async (sender) =>
            {
                await DisplayAlert("Error", "The settings for this device is not yet configured. Please do so first before continuing", "Ok");
                return;
            });

            MessagingCenter.Subscribe<SecondPageViewModel>(this, "NetworkAlert", async (sender) =>
            {
                await DisplayAlert("Network Alert", "No network", "Ok");
            });


            BtnSelectPromo.Clicked += BtnSelectPromo_Clicked;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.GetData.Execute(null);
        }

        private async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(((ListView)sender).SelectedItem is ProductDetail product))
                return;

            Analytics.TrackEvent("Product detail clicked", new Dictionary<string, string> {
               { "Product Name",product.Barcode },
               { "Product Image Url", product.FullDescription}
            });

            await Navigation.PushAsync(new SecondDetailPage(product));

            ((ListView)sender).SelectedItem = null;
        }

        private async void BtnSelectPromo_Clicked(object sender, EventArgs e)
        {
            if (ViewModel.IsBusy)
                return;

            try
            {
                ViewModel.IsBusy = true;

                MessagingCenter.Subscribe<PromoSelectionPage, PromoSummary>(this, "PromoSelected", async (sender1, summary) =>
                {
                    if (ViewModel.IsBusy)
                        return;

                    try
                    {
                        ViewModel.IsBusy = true;

                        await Navigation.PopModalAsync();

                        MessagingCenter.Subscribe<PromoItemSelectionPage, PromoSelectionModel>(this, "PromoItemSelected", async (sender2, selection) =>
                        {
                            if (ViewModel.IsBusy)
                                return;

                            try
                            {
                                ViewModel.IsBusy = true;

                                await Navigation.PopModalAsync();

                                var successful = await Task.Run(() => ViewModel.ScanPromo(selection.Summary, selection.Details, 1));

                                if (!successful)
                                {
                                    await DisplayAlert("Error", "Unable to Process Promo", "Ok");
                                    return;
                                }
                                else
                                {
                                    await DisplayAlert("Success", $"Added Promo {selection.Summary.PromoDescription}", "Ok");
                                    return;
                                }

                            }
                            catch (Exception ex)
                            {
                                await DisplayAlert("Error", "Error in inputing Promo customization: " + ex.Message, "Ok");
                            }
                            finally
                            {
                                MessagingCenter.Unsubscribe<PromoItemSelectionPage, List<PromoCustomizationModel>>(this, "PromoItemSelected");
                                ViewModel.IsBusy = false;
                            }
                        });

                        var promoItemSelection = new PromoItemSelectionPage(summary, false);

                        await Navigation.PushModalAsync(promoItemSelection);

                    }
                    catch (Exception ex)
                    {
                        await DisplayAlert("Error", "Error in Selecting Promo: " + ex.Message, "Ok");
                    }
                    finally
                    {
                        MessagingCenter.Unsubscribe<PromoSelectionPage, PromoSummary>(this, "PromoSelected");
                        ViewModel.IsBusy = false;
                    }

                });

                await Navigation.PushModalAsync(new PromoSelectionPage());

            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "Error in Selecting Promo: " + ex.Message, "Ok");
            }
            finally
            {
                ViewModel.IsBusy = false;
            }



        }

        private async void BtnScanPromo_OnClicked(object sender, EventArgs e)
        {
            if (ViewModel.IsBusy)
                return;

            try
            {

                ViewModel.IsBusy = true;

                MessagingCenter.Subscribe<PromoScannerPage, string>(this, "PromoScanned", async (sender1, arg) =>
                {

                    if (ViewModel.IsBusy)
                        return;

                    try
                    {
                        ViewModel.IsBusy = true;

                        foreach (var item in App.Cart)
                        {
                            if (item.Voucher == null)
                                continue;

                            if (item.Voucher.VoucherCode.Trim().ToUpper() == arg.Trim().ToUpper())
                            {
                                await DisplayAlert("Error", $"Voucher {arg} was already redeemed for this transaction", "Ok");
                                return;
                            }
                        }

                        await Navigation.PopModalAsync();
                        var summary = await Task.Run(() => ViewModel.PromoService.GetPromoItems(arg));
                        
                        MessagingCenter.Subscribe<PromoItemSelectionPage, PromoSelectionModel>(this, "PromoItemSelected", async (sender2, selection) =>
                        {
                            if (ViewModel.IsBusy)
                                return;

                            try
                            {

                                ViewModel.IsBusy = true;
                                await Navigation.PopModalAsync();
                                var successful = await Task.Run(() => ViewModel.ScanPromo(selection.Summary, selection.Details, 1));

                                if (!successful)
                                {
                                    await DisplayAlert("Error", "Unable to Process Promo", "Ok");
                                    return;
                                }
                                else
                                {
                                    await DisplayAlert("Success", $"Added Promo {selection.Summary.PromoDescription}", "Ok");
                                    return;
                                }
                            }
                            catch (Exception ex)
                            {
                                await DisplayAlert("Error", "Error in Selecting Promo: " + ex.Message, "Ok");

                            }
                            finally
                            {
                                MessagingCenter.Unsubscribe<PromoItemSelectionPage, PromoSelectionModel>(this, "PromoItemSelected");
                                ViewModel.IsBusy = false;
                            }
                        });

                        var promoItemSelection = new PromoItemSelectionPage(summary, true);                        
                        await Navigation.PushModalAsync(promoItemSelection);
                        return;

                    }
                    catch (Exception ex)
                    {
                        await DisplayAlert("Error", "Error in Scanning Promo: " + ex.Message, "Ok");

                    }
                    finally
                    {
                        MessagingCenter.Unsubscribe<PromoScannerPage, string>(this, "PromoScanned");
                        ViewModel.IsBusy = false;
                    }

                });

                await Navigation.PushModalAsync(new PromoScannerPage());

            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "Error in Scanning Promo: " + ex.Message, "Ok");
            }
            finally
            {
                ViewModel.IsBusy = false;
            }
        }
    }
}