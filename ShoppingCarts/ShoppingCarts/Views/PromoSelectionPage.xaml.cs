﻿using System;
using Microsoft.AppCenter.Analytics;
using ShoppingCarts.Model;
using ShoppingCarts.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShoppingCarts.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PromoSelectionPage : ContentPage
    {
        public PromoSelectionViewModel ViewModel;

        public PromoSelectionPage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new PromoSelectionViewModel();
            ItemsListView.ItemTapped += ItemsListView_ItemTapped;
        }

        private async void ItemsListView_ItemTapped(object sender, EventArgs e)
        {
            if (ViewModel.IsBusy)
                return;

            try
            {
                ViewModel.IsBusy = true;

                if (ViewModel.SelectedItem == null)
                {
                    await DisplayAlert("Error", "Please select a promo to add", "Ok");
                    return;
                }

                MessagingCenter.Send<PromoSelectionPage, PromoSummary>(this, "PromoSelected", ViewModel.SelectedItem);

            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "Error Adding Promo!: " + ex.Message, "Ok");
                return;
            }
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.GetData.Execute(null);
            ViewModel.IsBusy = false;
        }
    }
}