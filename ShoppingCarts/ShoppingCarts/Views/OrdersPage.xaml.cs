﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShoppingCarts.Helpers;
using ShoppingCarts.Model;
using ShoppingCarts.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrdersPage : ContentPage
    {
        bool isLoading;
        int position = 0;

        public OrdersPage()
        {
            InitializeComponent();

            ViewModel.Position = position;
            StatusPicker.SelectedIndex = 2;
            PickerTransactionDate.Date = DateTime.Now.Date;

            ViewModel.Status = (Constants.OrderStatus)StatusPicker.SelectedIndex;
            ViewModel.TransactionDate = PickerTransactionDate.Date;

            Task.Run(async () => { await ViewModel.GetSalesTask(); });

            BindingContext = ViewModel;

            ItemsListView.ItemTapped += ItemsListView_ItemTapped;
            ItemsListView.ItemAppearing += ItemsListView_ItemAppearing;
            StatusPicker.SelectedIndexChanged += StatusPicker_SelectedIndexChanged;

        }

        private async void ItemsListView_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            if (isLoading || ViewModel.Orders.Count == 0)
                return;

            var lastItem = (SalesSummary)e.Item;

            if (lastItem.SalesInvoiceNumber == ViewModel.Orders[ViewModel.Orders.Count - 1].SalesInvoiceNumber)
            {
                isLoading = true;
                position += Constants.OrderPageSize;
                ViewModel.Position = position;
                await ViewModel.GetSalesTask();
                isLoading = false;
            }

        }

        private async void ItemsListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selectedItem = (SalesSummary)e.Item;
            if (selectedItem == null)
                return;

            try
            {
                var result = (from ss in ViewModel.Orders
                              where ss.SalesInvoiceNumber == selectedItem.SalesInvoiceNumber
                              select ss).SingleOrDefault();

                await Navigation.PushAsync(new OrderPageDetail(result));

                MessagingCenter.Subscribe<OrderPageDetail, string>(this, "UpdateOrderStatus", async (sender1, arg) =>
                {
                    await Refresh();
                });


                MessagingCenter.Subscribe<OrderSuccessPage>(this, "UpdateOrderStatusFromOrderDetail", async (sender1) =>
                {
                    await Refresh();
                });

            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "Error viewing Order: " + ex.Message, "Ok");
                return;
            }

        }

        public OrdersPageViewModel ViewModel { get; set; } = new OrdersPageViewModel();

        private async void StatusPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            await Refresh();
        }

        private async void BtnRefresh_OnClicked(object sender, EventArgs e)
        {
            await Refresh();
        }

        private async void PickerTransactionDate_DateSelected(object sender, DateChangedEventArgs e)
        {
            await Refresh();
        }

        private async Task Refresh()
        {
            position = 0;
            ViewModel.Position = position;
            ViewModel.Status = (Constants.OrderStatus)StatusPicker.SelectedIndex;
            ViewModel.TransactionDate = PickerTransactionDate.Date;
            ViewModel.Orders.Clear();
            await Task.Run(async () => { await ViewModel.GetSalesTask(); });
            BindingContext = ViewModel;
        }

    }
}