﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing;
using ZXing.Mobile;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PromoScannerPage : TabbedPage
    {
        private string voucherCode;
        public string VoucherCode
        {
            get => voucherCode;
            set
            {
                if (voucherCode != value)
                {
                    voucherCode = value;
                }
            }
        }

        public View ContentHolder { get; set; }
        public PromoScannerPage()
        {
            InitializeComponent();

            ScannerView.Options = new MobileBarcodeScanningOptions()
            {
                PossibleFormats = new List<BarcodeFormat>() { BarcodeFormat.QR_CODE },
                AutoRotate = false,
                TryHarder = false,
                UseFrontCameraIfAvailable = false,
                TryInverted = false

            };

            ScannerView.OnScanResult += Handle_OnScanResult;
            ScannerOverlay.FlashButtonClicked += ScannerOverlay_FlashButtonClicked;
            ScannerView.AutoFocusRequested += ScannerView_AutoFocusRequested;

            BtnConfirm.Clicked += BtnConfirm_Clicked;
            BindingContext = this;
        }

        private void BtnConfirm_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(voucherCode))
            {
                DisplayAlert("Error", "Please type the voucher code!.", "Ok");
                return;
            }

            MessagingCenter.Send<PromoScannerPage, string>(this, "PromoScanned", voucherCode);

            Navigation.PopModalAsync();
        }

        private void ScannerView_AutoFocusRequested(int arg1, int arg2)
        {
            ScannerView.AutoFocus(arg1, arg2);
        }

        private void Handle_OnScanResult(Result result)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                ScannerView.IsScanning = false;
                ScannerView.IsAnalyzing = false;

                if (App.LoggedInUserId == null)
                    return;
                
                if (string.IsNullOrWhiteSpace(result.Text)) return;

                Vibration.Vibrate();                
                MessagingCenter.Send<PromoScannerPage, string>(this, "PromoScanned", result.Text);

                Navigation.PopModalAsync();

            });
        }

        protected override void OnAppearing()
        {

            base.OnAppearing();

            //ContentHolder = Content;
            //Content = null;
            //Content = ContentHolder;

            ScannerView.IsScanning = true;
            ScannerView.IsAnalyzing = true;

        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            ScannerView.IsScanning = false;
            ScannerView.IsAnalyzing = false;            
        }

        private void ScannerOverlay_FlashButtonClicked(Button sender, EventArgs e)
        {
            ScannerView.ToggleTorch();
        }
    }
}