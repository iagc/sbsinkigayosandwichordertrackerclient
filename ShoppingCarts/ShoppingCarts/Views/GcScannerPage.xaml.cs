﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing;
using ZXing.Mobile;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GcScannerPage : ContentPage
    {
        public View ContentHolder { get; set; }
        public GcScannerPage()
        {
            InitializeComponent();

            ScannerView.Options = new MobileBarcodeScanningOptions()
            {
                PossibleFormats = new List<BarcodeFormat>() { BarcodeFormat.QR_CODE },
                //PossibleFormats = new List<BarcodeFormat>() { BarcodeFormat.CODE_128 },
                AutoRotate = false,
                TryHarder = false,
                UseFrontCameraIfAvailable = false,
                TryInverted = false
            };

            ScannerView.OnScanResult += Handle_OnScanResult;
            ScannerOverlay.FlashButtonClicked += ScannerOverlay_FlashButtonClicked;

        }

        private void Handle_OnScanResult(Result result)
        {
            Device.BeginInvokeOnMainThread(() => 
            {
                if (App.LoggedInUserId == null)
                    return;

                if (string.IsNullOrWhiteSpace(result.Text)) return;

                ScannerView.IsScanning = false;
                ScannerView.IsAnalyzing = false;

                Vibration.Vibrate();
                MessagingCenter.Send<GcScannerPage, string>(this, "GcScanned", result.Text);

                Navigation.PopModalAsync();

            });
        }

        protected override void OnAppearing()
        {

            base.OnAppearing();

            ContentHolder = Content;
            Content = null;
            Content = ContentHolder;

            ScannerView.IsScanning = true;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            ScannerView.IsScanning = false;

        }

        private void ScannerOverlay_FlashButtonClicked(Button sender, EventArgs e)
        {
            ScannerView.ToggleTorch();
        }
    }
}