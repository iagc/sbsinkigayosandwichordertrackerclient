﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ShoppingCarts.Helpers.Interface;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPage : ContentPage
    {
        public ListView ListView => listView;

        public MasterPage()
        {
            InitializeComponent();
            string v = DependencyService.Get<IAppVersion>().GetVersion();
            int b = DependencyService.Get<IAppVersion>().GetBuild();

            LblVersion.Text = $"Version Number: {b}";
        }

        private void OnCartImageTapped(object sender, EventArgs e)
        {
            MessagingCenter.Send(this, "GoToCartDetailPage");
            
            ((MasterDetailPage)Parent).IsPresented = false;
        }
    }
}