﻿using System;
using Microsoft.AppCenter.Analytics;
using ShoppingCarts.Model;
using ShoppingCarts.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShoppingCarts.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;


namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EmployeeFoodAllowanceListPage : ContentPage
    {
        public EmployeeFoodAllowanceViewModel ViewModel;
        public EmployeeFoodAllowanceListPage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new EmployeeFoodAllowanceViewModel();
            ItemsListView.ItemTapped += BtnAdd_Clicked;
            BtnSearch.Clicked += BtnSearch_Clicked;
            EntrySearch.Completed += EntrySearch_Completed;
        }

        private void EntrySearch_Completed(object sender, EventArgs e)
        {
            Search();
        }

        private void BtnSearch_Clicked(object sender, EventArgs e)
        {
            Search();
        }

        private void Search()
        {
            ViewModel.GetData.Execute(EntrySearch.Text);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Search();
        }

        private async void BtnAdd_Clicked(object sender, ItemTappedEventArgs e)
        {
            if (ViewModel.IsBusy)
                return;
            try
            {
                ViewModel.IsBusy = true;

                if (ViewModel.SelectedItem == null)
                {
                    await DisplayAlert("Error", "Please select an employee!", "Ok");
                    return;
                }

                if (ViewModel.SelectedItem.HasAvailed == false)
                {
                    MessagingCenter.Send<EmployeeFoodAllowanceListPage, EmployeeFoodAllowanceModel>(this, "EFAPicked", ViewModel.SelectedItem);
                }
                else
                {
                    await DisplayAlert("Error", ViewModel.SelectedItem.EmployeeName + " already availed their food allowance today!","Ok");
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "Error Adding Food Alowance!: " + ex.Message, "Ok");
                return;
            }
            
        }
    }
}