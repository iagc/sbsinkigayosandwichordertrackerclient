﻿using System;
using Microsoft.AppCenter.Analytics;
using ShoppingCarts.Model;
using ShoppingCarts.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShoppingCarts.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PromoItemSelectionPage : ContentPage
    {
        public bool DisablePromoQty { get; set; }
        public PromoItemSelectionViewModel ViewModel;
        public PromoItemSelectionPage(PromoSummary promo, bool disablePromoQty)
        {
            InitializeComponent();
            BindingContext = ViewModel = new PromoItemSelectionViewModel();
            ViewModel.Promo = promo;
            ViewModel.DisablePromoQty = disablePromoQty;
            BtnConfirm.Clicked += BtnConfirm_Clicked;
        }

        private async void BtnConfirm_Clicked(object sender, EventArgs e)
        {
            if (ViewModel.IsBusy)
                return;

            try
            {
                ViewModel.IsBusy = true;

                int totalqty = 0;
                var result = ViewModel.Output.Sum(item => item.Quantity);

                var resultsummary = ViewModel.Output
                       .GroupBy(x => new { x.bundlename, x.totalqty })
                       .Select(g => new
                       {
                           BundleName = g.Key.bundlename,
                           g.Key.totalqty
                       });
                resultsummary.ToList();

                foreach (var info in resultsummary)
                {
                    totalqty += info.totalqty;
                }

                if (result < totalqty)
                {
                    ViewModel.IsBusy = false;
                    await DisplayAlert("Error", "Kindly fill up all the quantity of the free items!", "Ok");
                }
                else
                {
                    MessagingCenter.Send<PromoItemSelectionPage, PromoSelectionModel>(this, "PromoItemSelected", new PromoSelectionModel() { Summary = ViewModel.Promo, Details = ViewModel.Output });
                }

            }
            catch (Exception ex)
            {
                ViewModel.IsBusy = false;
                await DisplayAlert("Error", "Error in Clicking Promo Customization Confirmation: " + ex.Message, "Ok");
            }

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.IsBusy = false;
            ViewModel.GetData.Execute(null);
        }

        private void Entry_Unfocused(object sender, FocusEventArgs e)
        {
            if (e == null)
                return;

            if (!e.IsFocused)
            {
                if (sender == null)
                    return;

                var t = (Entry)sender;
                if (int.TryParse(t.Text, out int result))
                {
                    if (result == 0)
                    {
                        t.Text = "1";
                    }
                }
                else
                {
                    t.Text = "1";
                }
                
            }

        }

    }
}