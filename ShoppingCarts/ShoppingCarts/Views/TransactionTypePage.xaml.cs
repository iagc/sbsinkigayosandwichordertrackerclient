﻿using System;
using Microsoft.AppCenter.Analytics;
using ShoppingCarts.Model;
using ShoppingCarts.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShoppingCarts.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TransactionTypePage : ContentPage
    {
        public TransactionTypeViewModel ViewModel;
        public TransactionTypePage()
        {
            InitializeComponent();

            BindingContext = ViewModel = new TransactionTypeViewModel();
            ItemsListView.ItemTapped += BtnAdd_Clicked;
        }

        private async void BtnAdd_Clicked(object sender, EventArgs e)
        {

            if (IsBusy)
                return;

            try
            {
                IsBusy = true;                

                if (ViewModel.SelectedItem == null)
                {
                    await DisplayAlert("Error", "Please select a promo to add", "Ok");
                    return;
                }

                MessagingCenter.Send<TransactionTypePage, TransactionType>(this, "TransactionTypePicked", ViewModel.SelectedItem);

            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "Error Adding Promo!: " + ex.Message, "Ok");
                return;
            }
            finally
            {
                IsBusy = false;                
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.GetData.Execute(null);
        }
    }
}