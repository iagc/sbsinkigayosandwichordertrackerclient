﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ShoppingCarts.Helpers;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : ContentPage
    {
        public string HostName { get; set; }
        public string TerminalNumber { get; set; }
        public string AutoGenerateQueueNumber { get; set; }
        public string AppScenario { get; set; }

        public TimeSpan HappyHourStartTime { get; set; }
        public TimeSpan HappyHourEndTime { get; set; }
        public int HappyHourPromoId { get; set; }
        public bool HappyHourEnabled { get; set; }
        public bool ForceCrpSelection { get; set; }

        public SettingsPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            try
            {
                HostName = App.Settings.HostName;
                TerminalNumber = App.Settings.TerminalNumber;
                AutoGenerateQueueNumber = App.Settings.AutoGenerateQueueNumber;
                AppScenario = App.Settings.Scenario;

                HappyHourStartTime = App.Settings.HappyHourStartTime;
                HappyHourEndTime = App.Settings.HappyHourEndTime;
                HappyHourPromoId = App.Settings.HappyHourPromoId;
                HappyHourEnabled = App.Settings.HappyHourEnabled;
                ForceCrpSelection = App.Settings.ForceCrpSelection;

                BindingContext = this;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        private async void Button_OnClicked(object sender, EventArgs e)
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;

                if (EntryHostName.Text.Trim() == string.Empty)
                {
                    await DisplayAlert("Error", "Please provide value for the Host Name", "Ok");
                    return;
                }
                if (EntryTerminalNumber.Text.Trim() == string.Empty)
                {
                    await DisplayAlert("Error", "Please provide value for the Terminal Number", "Ok");
                    return;
                }

                if (AutoGenerateQueueNumberPicker.SelectedItem == null)
                {
                    await DisplayAlert("Error", "Please provide value for the Auto Generate Queue Number", "Ok");
                    return;
                }
                if (AppScenarioPicker.SelectedItem == null)
                {
                    await DisplayAlert("Error", "Please provide value for the App Scenario", "Ok");
                    return;
                }

                if (PickerStartTime.Time == null)
                {
                    await DisplayAlert("Error", "Please provide value for Happy Hour Start Time", "Ok");
                    return;
                }

                if (PickerEndTime.Time == null)
                {
                    await DisplayAlert("Error", "Please provide value for Happy Hour End Time", "Ok");
                    return;
                }

                if (EntryPromoID.Text.Trim() == "0" || EntryPromoID.Text.Trim() == "")
                {
                    await DisplayAlert("Error", "Please provide value for Happy Hour Promo ID", "Ok");
                    return;
                }

                await SecureStorage.SetAsync(Constants.HostNameKey, EntryHostName.Text.Trim());
                await SecureStorage.SetAsync(Constants.TerminalNumberKey, EntryTerminalNumber.Text.Trim());
                await SecureStorage.SetAsync(Constants.AutoGenerateQueueNumberKey, AutoGenerateQueueNumberPicker.SelectedItem.ToString());
                await SecureStorage.SetAsync(Constants.AppScenarioKey, AppScenarioPicker.SelectedItem.ToString());

                await SecureStorage.SetAsync(Constants.HappyHourStartTimeKey, PickerStartTime.Time.Ticks.ToString());
                await SecureStorage.SetAsync(Constants.HappyHourEndTimeKey, PickerEndTime.Time.Ticks.ToString());
                await SecureStorage.SetAsync(Constants.HappyHourPromoIdKey, EntryPromoID.Text.ToString());

                await SecureStorage.SetAsync(Constants.HappyHourEnabledKey, SwitchEnableHappyHour.IsToggled.ToString());                
                await SecureStorage.SetAsync(Constants.ForceCrpSelectionKey, SwitchForceCRPSelection.IsToggled.ToString());                

                App.Settings.HostName = EntryHostName.Text.Trim();
                App.Settings.TerminalNumber = EntryTerminalNumber.Text.Trim();
                App.Settings.AutoGenerateQueueNumber = AutoGenerateQueueNumberPicker.SelectedItem.ToString();
                App.Settings.Scenario = AppScenarioPicker.SelectedItem.ToString();

                App.Settings.HappyHourStartTime = PickerStartTime.Time;
                App.Settings.HappyHourEndTime = PickerEndTime.Time;
                App.Settings.HappyHourPromoId = Convert.ToInt32(EntryPromoID.Text);
                App.Settings.HappyHourEnabled = SwitchEnableHappyHour.IsToggled;
                App.Settings.ForceCrpSelection = SwitchForceCRPSelection.IsToggled;

                await DisplayAlert("Notice", "Successfully Updated Settings", "Ok");
                await Navigation.PopToRootAsync();

            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "Error in saving settings: " + ex.Message, "Ok");                
            }
            finally
            {
                IsBusy = false;
            }
        }

    }
}