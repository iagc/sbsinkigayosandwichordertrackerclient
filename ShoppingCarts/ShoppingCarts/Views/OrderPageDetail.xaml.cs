﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShoppingCarts.Model;
using ShoppingCarts.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderPageDetail : TabbedPage
    {

        public OrderPageDetail(SalesSummary order)
        {
            InitializeComponent();
            ViewModel.Order = order;
            DetailsListView.ItemsSource = ViewModel.Order.Salesdetails;
            PaymentsListView.ItemsSource = ViewModel.Order.Salespayments;            
            LblSubTotal.Text = ViewModel.Order.Salesdetails.Sum(x => x.xSubTotal).ToString("N2");
            LblDiscountAmount.Text = ViewModel.Order.FDiscountAmount.ToString("N2");
            LblTotal.Text = ViewModel.Order.Salesdetails.Sum(x => x.NetPerItem).Value.ToString("N2");
            LblTendered.Text = ViewModel.Order.Salespayments.Sum(x => x.Amount).Value.ToString("N2");
            BtnServe.Clicked += BtnServe_Clicked;
            BtnVoid.Clicked += BtnVoid_Clicked;

            if (ViewModel.Order.Migrated)
            {
                BtnVoid.IsEnabled = false;
                BtnServe.IsEnabled = false;
                BtnVoid.Text = "Cannot void, already served";
                BtnServe.Text = "Cannot serve, already served";
            }

            if (ViewModel.Order.SalesIsVoid == 1)
            {
                BtnVoid.IsEnabled = false;
                BtnServe.IsEnabled = false;
                BtnVoid.Text = "Cannot void, already void";
                BtnServe.Text = "Cannot serve, already void";
            }
            BindingContext = ViewModel;
        }

        private async void BtnVoid_Clicked(object sender, EventArgs e)
        {
            if (ViewModel.IsBusy)
                return;

            try
            {
                ViewModel.IsBusy = true;

                var res = await DisplayAlert("Confirm", $"Are you sure you want to VOID {ViewModel.Order.SalesInvoiceNumber}?", "Ok", "Cancel");

                if (res)
                {
                    var result = await Task.Run(() => ViewModel.VoidOrder(ViewModel.Order.SalesInvoiceNumber));
                    if (!result) return;
                    await DisplayAlert("Order Server", "Order " + ViewModel.Order.SalesInvoiceNumber + " has been voided", "Ok");
                    await Navigation.PopAsync();
                    MessagingCenter.Send<OrderPageDetail, string>(this, "UpdateOrderStatus", "Void");
                }

            }
            catch (Exception ex)
            {

                await DisplayAlert("Error", "Error in voiding order: " + ex.Message, "Ok");
            }
            finally
            {
                ViewModel.IsBusy = false;
            }

        }        

        private async void BtnServe_Clicked(object sender, EventArgs e)
        {
            if (ViewModel.IsBusy)
                return;

            try
            {
                ViewModel.IsBusy = true;

                if (ViewModel.Order.SalesType == 1)
                {
                    App.Cart.Clear();
                    foreach (var detail in ViewModel.Order.Salesdetails)
                    {
                        App.Cart.Add(new ProductDetail
                        {
                            Barcode = detail.Barcode,
                            Category = string.Empty,
                            FullDescription = detail.FullDescription,
                            OrigPrice = detail.OrigPrice,
                            Price = detail.SalePrice,
                            PromoDescription = string.Empty,
                            PromoID = detail.DiscountRef,
                            Quantity = detail.Quantity,
                            ShortDescription = detail.FullDescription,
                            Status = 0,
                            Subtotal = detail.SalePrice * detail.Quantity,
                            Voucher = null
                        });
                    }

                    var chkOut = new CheckOutPage();
                    chkOut.ViewModel.IsFromOrderDetail = true;
                    chkOut.ViewModel.InvoiceNumber = ViewModel.Order.SalesInvoiceNumber;
                    chkOut.ViewModel.IsPreOrder = ViewModel.Order.SalesType == 1 ? true : false;
                    await Navigation.PushAsync(chkOut, true);
                }
                else
                {
                    var result = await Task.Run(() => ViewModel.ServeOrder(ViewModel.Order.SalesInvoiceNumber));
                    if (!result) return;
                    await DisplayAlert("Order Server", "Order " + ViewModel.Order.SalesInvoiceNumber + " has been served", "Ok");
                    await Navigation.PopAsync();
                    MessagingCenter.Send<OrderPageDetail, string>(this, "UpdateOrderStatus", "Serve");
                }

            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "Error in serving Order: " + ex.Message, "Ok");

            }
            finally
            {
                ViewModel.IsBusy = false;
            }

        }

        public OrderPageDetailViewModel ViewModel { get; set; } = new OrderPageDetailViewModel();

    }
}