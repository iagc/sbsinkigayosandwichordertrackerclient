﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ShoppingCarts.ViewModels;
using Xamarin.Essentials;
using ShoppingCarts.Helpers;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {

        public LoginPage()
        {
            InitializeComponent();

            BindingContext = ViewModel;

            UsernameEntry.Completed += (sender, args) => { PasswordEntry.Focus(); };
            UsernameEntry.TextChanged += (sender, args) =>
            {
                LblAppConfig.IsVisible = UsernameEntry.Text.Trim() == "001";
                AppConfigPicker.IsVisible = UsernameEntry.Text.Trim() == "001";
            };

            PasswordEntry.Completed += async (sender, args) =>
            {
                await Login();
            };

        }
        public LoginPageViewModel ViewModel { get; set; } = new LoginPageViewModel();

        private async void ContentPage_Appearing(object sender, EventArgs e)
        {

            AppConfigPicker.IsVisible = false;
            LblAppConfig.IsVisible = false;

            App.Settings.AppConfig = await SecureStorage.GetAsync(Constants.AppConfigKey);
            if (App.Settings.AppConfig != null && App.Settings.AppConfig.Trim() != string.Empty)
            {
                AppConfigPicker.SelectedItem = App.Settings.AppConfig;
            }

        }

        private async Task Login()
        {
            if (AppConfigPicker.SelectedItem == null)
            {
                await DisplayAlert("Error", "Please set App Config before logging in", "Ok");
                return;
            }

            await SecureStorage.SetAsync(Constants.AppConfigKey, AppConfigPicker.SelectedItem.ToString());
            App.Settings.AppConfig = AppConfigPicker.SelectedItem.ToString();

            ViewModel.AuthenticateCommand.Execute(null);
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await Login();
        }
    }
}