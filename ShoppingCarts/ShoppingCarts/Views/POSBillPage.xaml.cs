﻿using System;
using Microsoft.AppCenter.Analytics;
using ShoppingCarts.Model;
using ShoppingCarts.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShoppingCarts.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PosBillPage : ContentPage
    {
        public PosBillPageViewModel ViewModel;
        public PosBillPage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new PosBillPageViewModel();

            BtnSave.BindingContext = this;
            BtnSave.Clicked += BtnSaveOnClicked;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.GetData.Execute(null);
        }

        private async void BtnSaveOnClicked(object sender, EventArgs e)
        {
            var isOk = AreSettingsSet();

            if (!isOk)
            {
                await DisplayAlert(
                    "Error", "Settings are not configured correctly. Please input the required settings before you can Place an Order", "Ok");
                return;
            }

            var result = await Task.Run(() => ViewModel.SavePosBill());

            if (result == true)
            {
                await DisplayAlert("Success", "POS Bill Successfully Saved!", "Ok");
            }
            else
            {
                await DisplayAlert("Error", "An error occured.", "Ok");
            }

        }

        private static bool AreSettingsSet()
        {
            var hostName = App.Settings.HostName;
            var terminalNumber = App.Settings.TerminalNumber;

            return hostName.Trim() != string.Empty && terminalNumber.Trim() != string.Empty;
        }

        private void Entry_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (((Entry)sender).Text.Trim() == "")
            {
                ((Entry)sender).Text = "0";
            }

            GetGrandTotal();
        }

        private void GetGrandTotal()
        {
            var total = ViewModel.Details.Sum(o => o.Subtotal);
            GrandTotal.Text = $"Total: {total:N2}";
        }
    }
}