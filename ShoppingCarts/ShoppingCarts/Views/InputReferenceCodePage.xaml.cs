﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InputReferenceCodePage : ContentPage
    {

        public string ReferenceCode { get; set; }
        //public string TransactionTypeDescription { get; set; }
        public string InputMessage { get; set; }
        public string InputPlaceHolder { get; set; }
        public InputReferenceCodePage()
        {
            InitializeComponent();

            BtnConfirm.Clicked += BtnConfirm_Clicked;

            BindingContext = this;
        }

        private async void BtnConfirm_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(ReferenceCode))
            {
                await DisplayAlert("Error", "Please type the reference code!.", "Ok");
                return;
            }

            MessagingCenter.Send<InputReferenceCodePage, string>(this, "ReferenceCodeTyped", ReferenceCode);
        }
    }
}