﻿using ShoppingCarts.ViewModels;
using Xamarin.Forms;
using System;
using Xamarin.Forms.Xaml;
using System.Threading.Tasks;
using Xamarin.Essentials;
using ShoppingCarts.Helpers;
using ShoppingCarts.Utilities;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CartDetailPage : ContentPage
    {
        public CartDetailPageViewModel _ViewModel;

        public CartDetailPage()
        {
            InitializeComponent();

            BindingContext = _ViewModel = new CartDetailPageViewModel();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            _ViewModel.GetData.Execute(null);

            var isOK = SettingsChecker.AreSettingsSet();

            if (isOK)
            {
                var scenario = App.Settings.Scenario;

                if (scenario.ToLowerInvariant() == "preorder")
                {
                    LblQueueNumber.IsVisible = true;
                    EntryQueueNumber.IsVisible = true;
                }
                else
                {
                    LblQueueNumber.IsVisible = false;
                    EntryQueueNumber.IsVisible = false;
                }                

            }
            else
            {
                await Navigation.PopAsync();
                await DisplayAlert("Error", "The settings for this device is not yet configured. Please do so first before continuing", "Ok");                
            }


            
        }

        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }

        private async void ClickCheckOut(object sender, System.EventArgs e)
        {

            BtnCheckout.IsEnabled = false;

            if (App.Cart.Count <= 0)
            {
                await DisplayAlert("Error", "There are no items in the Cart", "Ok");
                return;
            }

            var isOK = SettingsChecker.AreSettingsSet();

            if (!isOK)
            {
                await DisplayAlert("Error", "The settings for this device is not yet configured. Please do so first before continuing", "Ok");
                return;
            }

            try
            {
                var scenario = App.Settings.Scenario;

                if (_ViewModel.QueueNumber == 0 && scenario.ToLowerInvariant() == "preorder")
                {
                    await DisplayAlert("Error", "Please specify Queue Number", "Ok");
                    return;
                }

                if (scenario.ToLowerInvariant() == "preorder")
                {
                    var result = await Task.Run(() => _ViewModel.PlaceOrder());
                    if (result.Success)
                    {
                        await Navigation.PushAsync(
                            new OrderSuccessPage(result.Info.SalesInvoiceNumber, result.Info.QueueNumber, result.Info.ChangeAmount, true, false, false), true);
                    }
                }
                else
                {
                    await Navigation.PushAsync(new CheckOutPage());
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "An Exception occured while saving Pre Order: " + ex.Message, "Ok");
            }
            finally
            {
                BtnCheckout.IsEnabled = true;
            }
        }
        
    }
}