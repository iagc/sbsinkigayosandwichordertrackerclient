﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InputPromoQuantityPage : ContentPage
    {
        public int PromoQty { get; set; } = 1;
        public string PromoDescription { get; set; }
        public InputPromoQuantityPage()
        {
            InitializeComponent();

            BtnConfirm.Clicked += BtnConfirm_Clicked;
            BindingContext = this;
        }

        private async void BtnConfirm_Clicked(object sender, EventArgs e)
        {
            if (PromoQty == 0)
            {
                await DisplayAlert("Error", "Promo quantity cannot be 0!.", "Ok");
                return;
            }

            MessagingCenter.Send<InputPromoQuantityPage, int>(this, "PromoQuantityTyped", PromoQty);
        }

        private void Entry_Unfocused(object sender, FocusEventArgs e)
        {
            var txt = (Entry)sender;

            if (string.IsNullOrEmpty(txt.Text)
                || string.IsNullOrWhiteSpace(txt.Text)
                || txt.Text == "0")
            {
                txt.Text = "1";
            }
        }
    }
}