﻿using System;
using Microsoft.AppCenter.Analytics;
using ShoppingCarts.Model;
using ShoppingCarts.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using ShoppingCarts.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using ShoppingCarts.Utilities;
using System.Linq;

namespace ShoppingCarts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CheckOutPage : TabbedPage
    {
        public CheckOutPageViewModel ViewModel;

        public CheckOutPage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new CheckOutPageViewModel();

            MessagingCenter.Subscribe<CheckOutPageViewModel>(this, "NetworkAlert", async (sender) =>
            {
                await DisplayAlert("Network Alert", "No network", "Ok");
            });

            BtnSave.Clicked += BtnSaveOnClicked;
            BtnAddDiscount.Clicked += BtnAddDiscount_Clicked;
            BtnRemoveDiscount.Clicked += BtnRemoveDiscount_Clicked;
            BtnTransactionType.Clicked += BtnTransactionType_Clicked;
            BtnRemoveTransactionType.Clicked += BtnRemoveTransactionType_Clicked;
            BtnAddCRP.Clicked += BtnAddCRP_Clicked;
            BtnRemoveCRP.Clicked += BtnRemoveCRP_Clicked;

            ViewModel.CheckoutCart = new List<ProductDetail>();
            ViewModel.CheckoutCart = App.Cart.DeepCopy(true);
        }

        private void BtnRemoveCRP_Clicked(object sender, EventArgs e)
        {
            if (ViewModel.SalesCRP != null)
            {
                ViewModel.SalesCRP = null;
            }
        }

        private async void BtnAddCRP_Clicked(object sender, EventArgs e)
        {
            if (ViewModel.IsBusy)
                return;

            try
            {
                ViewModel.IsBusy = true;

                if (ViewModel.SalesCRP == null)
                {

                    MessagingCenter.Subscribe<SalesCRPListPage, CrpModel>(this, "SalesCRPPicked", async (sender1, arg) =>
                    {

                        if (ViewModel.IsBusy)
                            return;

                        try
                        {
                            ViewModel.IsBusy = true;

                            if (arg != null)
                            {
                                ViewModel.SalesCRP = arg;
                                await DisplayAlert("Sales CRP Added!", $"{arg.EmployeeNumber} is now been added as a Sales CRP!", "Ok");
                                await Navigation.PopModalAsync();
                                return;
                            }
                            else
                            {
                                await DisplayAlert("Error!", "Please select an employee!", "Ok");
                                return;
                            }

                        }
                        catch (Exception ex)
                        {
                            await DisplayAlert("Error", ex.Message, "Ok");
                        }
                        finally
                        {
                            MessagingCenter.Unsubscribe<SalesCRPListPage, CrpModel>(this, "SalesCRPPicked");
                            ViewModel.IsBusy = false;
                        }

                    });

                    await Navigation.PushModalAsync(new SalesCRPListPage());
                }
                else
                {
                    await DisplayAlert("Error!", "Please remove first the current Sales CRP!", "Ok");
                }

            }
            catch (Exception ex)
            {
                await DisplayAlert("Error!", "Error in Selecting CRP:" + ex.Message, "Ok");

            }
            finally
            {
                ViewModel.IsBusy = false;
            }
        }

        private async void BtnRemoveTransactionType_Clicked(object sender, EventArgs e)
        {

            string TransactionTypeDescription = string.Empty;

            if (ViewModel.TransactionType != null)
            {
                TransactionTypeDescription = ViewModel.TransactionType.TransactionDescription;
                ViewModel.TransactionType = null;
            }

            var isHappHourFound = false;
            ViewModel.CheckoutCart = new List<ProductDetail>();
            ViewModel.CheckoutCart = App.Cart.DeepCopy(true);

            foreach (var item in ViewModel.CheckoutCart)
            {
                var nowTimeSpan = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                if (nowTimeSpan >= App.Settings.HappyHourStartTime
                    && nowTimeSpan <= App.Settings.HappyHourEndTime
                    && (item.Barcode == "SBS00000015" || item.Barcode == "SBS00000145")
                    && item.PromoID.Trim() == ""
                    && App.Settings.HappyHourEnabled)
                {
                    isHappHourFound = true;
                }
            }

            if (isHappHourFound)
            {
                await DisplayAlert("Alert!", $"Transaction Type {TransactionTypeDescription} has been removed. Happy Hour discount is now reactivated!", "Ok");
            }

            ViewModel.GetData.Execute(null);
        }

        private async void BtnTransactionType_Clicked(object sender, EventArgs e)
        {

            if (ViewModel.IsBusy)
                return;

            try
            {

                if (ViewModel.TransactionType != null)
                {
                    await DisplayAlert("Error!", "Please remove first the current transaction type.", "Ok");
                    return;
                }

                if (ViewModel.DiscountType != null)
                {
                    await DisplayAlert("Error!", "You cannot set Transaction Type if you have a discount. " + Environment.NewLine +
                                                 "Please remove the current discount first.", "Ok");
                    return;
                }

                ViewModel.IsBusy = true;

                MessagingCenter.Subscribe<TransactionTypePage, TransactionType>(this, "TransactionTypePicked", async (sender1, arg) =>
                {
                    
                    if (ViewModel.IsBusy)
                        return;

                    try
                    {
                        ViewModel.IsBusy = true;

                        if (arg != null)
                        {
                            await Navigation.PopModalAsync();

                            if (arg.TransactionCode == "EFA")
                            {
                                MessagingCenter.Subscribe<EmployeeFoodAllowanceListPage, EmployeeFoodAllowanceModel>(this, "EFAPicked", async (sender3, employee) =>
                                {

                                    if (ViewModel.IsBusy)
                                        return;

                                    try
                                    {
                                        ViewModel.IsBusy = true;

                                        if (arg != null)
                                        {
                                            arg.ReferenceCode = employee.EmployeeNumber;
                                            ViewModel.TransactionType = arg;

                                            if (ViewModel.CheckoutCart != null)
                                            {
                                                foreach (var item in ViewModel.CheckoutCart)
                                                {
                                                    item.Price = 0;
                                                    item.PromoID = "";
                                                    item.PromoDescription = "";
                                                }
                                            }

                                            await DisplayAlert("Employee Food Allowance Activated!", $"{employee.EmployeeName} is now activated for Food Allowance!", "Ok");
                                            await Navigation.PopModalAsync();
                                            return;
                                        }
                                        else
                                        {
                                            await DisplayAlert("Error!", "Please select an employee to activate", "Ok");
                                            return;
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        await DisplayAlert("Error", ex.Message, "Ok");
                                    }
                                    finally
                                    {
                                        MessagingCenter.Unsubscribe<EmployeeFoodAllowanceListPage, EmployeeFoodAllowanceModel>(this, "EFAPicked");
                                        ViewModel.IsBusy = false;
                                    }
                                });

                                await Navigation.PushModalAsync(new EmployeeFoodAllowanceListPage());
                                return;
                            }
                            else
                            {
                                MessagingCenter.Subscribe<InputReferenceCodePage, string>(this, "ReferenceCodeTyped", async (sender2, refcode) =>
                                {
                                    if (ViewModel.IsBusy)
                                        return;

                                    try
                                    {

                                        if (string.IsNullOrWhiteSpace(refcode))
                                        {
                                            throw new Exception("Reference Code is blank");
                                        }

                                        ViewModel.IsBusy = true;
                                        arg.ReferenceCode = refcode;
                                        ViewModel.TransactionType = arg;

                                        if (arg.TransactionCode == "FT")
                                        {
                                            if (ViewModel.CheckoutCart != null)
                                            {
                                                foreach (var item in ViewModel.CheckoutCart)
                                                {
                                                    item.Price = 0;
                                                    item.PromoID = "";
                                                    item.PromoDescription = "";
                                                }
                                            }
                                        }

                                        await Navigation.PopModalAsync();
                                    }
                                    catch (Exception ex2)
                                    {
                                        await DisplayAlert("Error", "Error in handling Reference code: " + ex2.Message, "Ok");
                                    }
                                    finally
                                    {
                                        ViewModel.IsBusy = false;
                                        MessagingCenter.Unsubscribe<InputReferenceCodePage, string>(this, "ReferenceCodeTyped");
                                    }
                                });

                                string _inputmessage;
                                string _inputplaceholder;
                                _inputmessage = "";

                                if (arg.TransactionCode == "FT")
                                {
                                    _inputmessage = "Enter the name of the customer who will avail the " + arg.TransactionDescription;
                                    _inputplaceholder = "Customer Name";
                                }
                                else
                                {
                                    _inputmessage = "Enter the reference code for " + arg.TransactionDescription + " in the field below. Make sure that the code entered is the same as the one seen in the app for";
                                    _inputplaceholder = "Reference Code";
                                }

                                var inputReferencePage = new InputReferenceCodePage()
                                {
                                    InputMessage = _inputmessage,
                                    InputPlaceHolder = _inputplaceholder
                                };

                                await Navigation.PushModalAsync(inputReferencePage);
                                return;
                            }
                        }
                        else
                        {
                            await DisplayAlert("Error!", "Please select a Transaction Type to activate", "Ok");
                            return;
                        }

                    }
                    catch (Exception ex)
                    {
                        await DisplayAlert("Error", "Error in handling Transaction Type selection: " + ex.Message, "Ok");
                    }
                    finally
                    {
                        ViewModel.IsBusy = false;
                        MessagingCenter.Unsubscribe<TransactionTypePage, TransactionType>(this, "TransactionTypePicked");
                    }
                });

                await Navigation.PushModalAsync(new TransactionTypePage());
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "Error in Clicking Transaction Type selection: " + ex.Message, "Ok");
            }
            finally
            {
                ViewModel.IsBusy = false;
            }
        }

        private void BtnRemoveDiscount_Clicked(object sender, EventArgs e)
        {
            if (ViewModel.DiscountType != null)
            {
                ViewModel.DiscountType = null;
            }
        }

        private async void BtnAddDiscount_Clicked(object sender, EventArgs e)
        {
            if (ViewModel.IsBusy)
                return;

            try
            {

                if (ViewModel.TransactionType != null)
                {
                    await DisplayAlert("Error!", "You cannot set Discount if you have set the Transaction Type. " + Environment.NewLine +
                                                 "Please remove the current Transaction Type first", "Ok");
                    return;
                }

                ViewModel.IsBusy = true;

                if (ViewModel.DiscountType == null)
                {
                    MessagingCenter.Subscribe<DiscountTypesPage, DiscountType>(this, "DiscountPicked", async (sender1, arg) =>
                    {
                        if (ViewModel.IsBusy)
                            return;

                        try
                        {
                            ViewModel.IsBusy = true;

                            if (arg != null)
                            {
                                ViewModel.DiscountType = arg;
                                await DisplayAlert("Discount Activated!", $"{arg.Description} is now been activated!", "Ok");
                                await Navigation.PopModalAsync();
                                return;
                            }
                            else
                            {
                                await DisplayAlert("Error!", "Please select a discount to activate", "Ok");
                                return;
                            }

                        }
                        catch (Exception ex)
                        {
                            await DisplayAlert("Error", ex.Message, "Ok");
                        }
                        finally
                        {
                            MessagingCenter.Unsubscribe<DiscountTypesPage, DiscountType>(this, "DiscountPicked");
                            ViewModel.IsBusy = false;
                        }

                    });

                    await Navigation.PushModalAsync(new DiscountTypesPage(ViewModel.CheckOut.TotalAmount));
                }
                else
                {
                    await DisplayAlert("Error!", "Please remove first the current discount", "Ok");
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error!", "Error is selecting discount: " + ex.Message, "Ok");
            }
            finally
            {
                ViewModel.IsBusy = false;
            }
        }

        private async void BtnSaveOnClicked(object sender, EventArgs e)
        {
            if (ViewModel.IsBusy)
                return;

            try
            {

                ViewModel.AutoGenerateQueueNumber = App.Settings.AutoGenerateQueueNumber.ToLowerInvariant() == "yes" ? true : false;
                ViewModel.IsDirectToCashier = SwitchDirectToCashier.IsToggled;

                if (!ViewModel.AutoGenerateQueueNumber && ViewModel.QueueNumber == 0 && !ViewModel.IsDirectToCashier)
                {
                    await DisplayAlert("Error", "Please specify Tent Number", "Ok");
                    return;
                }

                if (ViewModel.CheckOut.TotalAmount == 0 && ViewModel.AmountTendered > 0)
                {
                    await DisplayAlert("Error", "You cannot specify amount tendered when total amount is 0", "Ok");
                    return;
                }

                if (ViewModel.TransactionType != null)
                {
                    if (ViewModel.TransactionType.TransactionCode == "EFA" && ViewModel.AmountTendered > 0)                    
                    {
                        await DisplayAlert("Error", "You cannot specify amount tendered when Food Allowance is active!", "Ok");
                        return;
                    }

                    if (ViewModel.TransactionType.TransactionCode == "FT" && ViewModel.AmountTendered > 0)
                    {
                        await DisplayAlert("Error", "You cannot specify amount tendered when Food Taste transaction is active!", "Ok");
                        return;
                    }
                }

                if (ViewModel.DiscountType != null)
                {
                    if (ViewModel.DiscountType.Code.ToLowerInvariant() == "scd"
                        || ViewModel.DiscountType.Code.ToLowerInvariant() == "pwd")
                    {
                        if (ViewModel.DiscountType.CustomerID.Trim() == string.Empty)
                        {
                            await DisplayAlert("Error", "You have added Senior/PWD Discount. Please provide the Senior/PWD ID No.", "Ok");
                            return;
                        }

                        if (ViewModel.DiscountType.CustomerName.Trim() == string.Empty)
                        {
                            await DisplayAlert("Error", "You have added Senior/PWD Discount. Please provide the Senior/PWD Name", "Ok");
                            return;
                        }
                    }
                }

                if (ViewModel.SalesCRP == null && App.Settings.ForceCrpSelection)
                {
                    await DisplayAlert("Error", "Please specify CRP", "Ok");
                    return;
                }

                var result = await Task.Run(() => ViewModel.PlaceOrder());
                if (result.Success)
                {
                    await Navigation.PushAsync(
                        new OrderSuccessPage(result.Info.SalesInvoiceNumber, result.Info.QueueNumber, result.Info.ChangeAmount, !ViewModel.IsDirectToCashier, true, true), true);
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", "Error in placing order: " + ex.Message, "Ok");
            }

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.GetData.Execute(null);

            var autoGenerateQueueNumberFromStorage = App.Settings.AutoGenerateQueueNumber;
            var autoGenerateQueueNumber = autoGenerateQueueNumberFromStorage.ToLowerInvariant() == "yes" ? true : false;

            EntryQueueNumber.IsVisible = !autoGenerateQueueNumber;

            if (ViewModel.IsFromOrderDetail)
            {
                SwitchDirectToCashier.IsEnabled = false;
                SwitchDirectToCashier.IsToggled = true;
            }
        }

        private void BtnScanGc_OnClicked(object sender, EventArgs e)
        {
            MessagingCenter.Subscribe<GcScannerPage, string>(this, "GcScanned", (sender1, arg) =>
            {

                ViewModel.GcNumber = "";
                ViewModel.GcAmount = 0;

                try
                {
                    var result = Task.Run(() => ViewModel.GetGc(arg));

                    if (result.Result.GCNumber.Length == 0)
                    {
                        DisplayAlert("Notice", "GC Number not found", "Ok");
                        return;
                    }

                    ViewModel.GcNumber = result.Result.GCNumber;

                    if (result.Result.GCValue == null)
                    {
                        ViewModel.GcAmount = 0;
                        return;
                    }

                    ViewModel.GcAmount = (double)result.Result.GCValue;
                }
                catch (Exception ex)
                {
                    DisplayAlert("Error", ex.Message, "Ok");
                }
                finally
                {
                    MessagingCenter.Unsubscribe<GcScannerPage, string>(this, "GcScanned");
                }

            });

            Navigation.PushModalAsync(new GcScannerPage());
        }

        private void SwitchDirectToCashier_Toggled(object sender, ToggledEventArgs e)
        {
            EntryQueueNumber.IsEnabled = !SwitchDirectToCashier.IsToggled;
            EntryQueueNumber.Text = "0";
        }

    }
}