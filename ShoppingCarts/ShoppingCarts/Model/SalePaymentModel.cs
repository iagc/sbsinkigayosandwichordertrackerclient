﻿namespace ShoppingCarts.Model
{

    public enum PaymentType
    {
        Cash = 0,
        Card = 1,
        Gc = 3
    }
    public class SalePaymentModel
    {
        public PaymentType Type { get; set; }
        public double Amount { get; set; }
        public string GcNumber { get; set; }

    }

}