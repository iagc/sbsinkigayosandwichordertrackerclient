﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public class OrderSuccess
    {
        public OrderSuccess(bool success, SalesResult info)
        {
            Success = success;
            Info = info;
        }

        public OrderSuccess()
        {
            
        }

        public bool Success { get; set; }
        public SalesResult Info { get; set; }
    }
}
