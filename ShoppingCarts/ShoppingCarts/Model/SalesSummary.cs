﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{

    public partial class SalesSummary
    {        
        public string SalesInvoiceNumber { get; set; }
        public System.DateTime SalesSetTransactionDate { get; set; }
        public System.DateTime SalesTransactionDate { get; set; }
        public string SalesTerminalNumber { get; set; }
        public string SalesTransactionType { get; set; }
        public string SalesTransactionDescription { get; set; }
        public string SalesCompany { get; set; }
        public string SalesStoreCode { get; set; }
        public string SalesTransactionNumber { get; set; }
        public string SalesEmployeeNumber { get; set; }
        public string SalesCRPNumber { get; set; }
        public string DRNumber { get; set; }
        public string CustomerPONo { get; set; }
        public string Bagger { get; set; }
        public string CustomerID { get; set; }
        public sbyte ReturnFlag { get; set; }
        public string SalesReturnStatus { get; set; }
        public sbyte SalesIsVoid { get; set; }
        public double FDiscountAmount { get; set; }
        public bool Migrated { get; set; }
        public string MemberCode { get; set; }
        public double? VAT { get; set; }
        public double? AdvancePayment { get; set; }
        public double? ChangeAmount { get; set; }
        public bool? CashedOut { get; set; }
        public bool? Uploaded { get; set; }
        public string MemberDiscount { get; set; }
        public bool? IsPointsAdded { get; set; }
        public long? AppliedPoints { get; set; }
        public long? EarnedPoints { get; set; }
        public string DiscountCode { get; set; }
        public string DiscountType { get; set; }
        public string DiscountRef { get; set; }
        public string DiscountedBy { get; set; }
        public string Discount { get; set; }
        public double? ServiceCharge { get; set; }
        public double? SCAmount { get; set; }
        public double? NonVatAmount { get; set; }
        public double? DiscountAmount { get; set; }
        public string EmployeeIDForSD { get; set; }
        public sbyte? Printed { get; set; }
        public DateTime? DatePrinted { get; set; }
        public string SICustomer { get; set; }
        public string SICustomerTIN { get; set; }
        public int? GuestCount { get; set; }
        public int? SeniorCount { get; set; }
        public sbyte? SalesType { get; set; }
        public int? TableNumber { get; set; }

        public virtual ICollection<SalesDetail> Salesdetails { get; set; }
        public virtual ICollection<SalesPayment> Salespayments { get; set; }
        public virtual SalesCustomerInfo Salescustomerinfo { get; set; }
        public virtual Terminal Terminal { get; set; }
    }
}
