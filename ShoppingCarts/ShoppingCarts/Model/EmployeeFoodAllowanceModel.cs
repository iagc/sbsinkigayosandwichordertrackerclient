﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public class EmployeeFoodAllowanceModel
    {
        public string EmployeeNumber { get; set; }
        public string EmployeeName { get; set; }

        public bool HasAvailed { get; set; }
    }
}
