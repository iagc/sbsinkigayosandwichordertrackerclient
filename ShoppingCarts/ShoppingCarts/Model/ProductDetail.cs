﻿using System;

namespace ShoppingCarts.Model
{

    [Serializable]
    public class ProductDetail
    {

        public string Barcode { get; set; }
        public string Category { get; set; }
        public string FullDescription { get; set; }
        public string ShortDescription { get; set; }
        public double OrigPrice { get; set; }
        public double Price { get; set; }
        public int Status { get; set; }        
        public int Quantity { get; set; } = 1;
        public double Subtotal { get; set; } = 0.00;
        public string PromoID { get; set; } = "";
        public string PromoDescription { get; set; } = "";
        public Voucher Voucher { get; set; } = new Voucher();
        public string CategorySort
        {
            get
            { 
                if (string.IsNullOrWhiteSpace(Category) || Category.Length == 0)
                    return "?";

                return Category.ToString().ToUpper();
            }
        }
    }
}