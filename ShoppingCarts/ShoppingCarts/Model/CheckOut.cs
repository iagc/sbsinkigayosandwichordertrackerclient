﻿using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public class CheckOutModel
    {
        public double SubTotalAmount { get; set; }
        public double TotalAmount { get; set; }
        public ObservableRangeCollection<ProductDetail> Products { get; } = new ObservableRangeCollection<ProductDetail>();

    }
}
