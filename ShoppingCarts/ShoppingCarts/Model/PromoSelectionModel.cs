﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    class PromoSelectionModel
    {
        public PromoSummary Summary { get; set; }
        public List<PromoCustomizationModel> Details { get; set; }
    }
}
