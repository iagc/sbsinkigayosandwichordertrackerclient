﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public class SaleModel
    {
        public string CashierEmployeeNumber { get; set; }
        public double TicketDiscount { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerContactNumber { get; set; }
        public string CustomerTin { get; set; }
        public string CustomerBusinessStyle { get; set; }
        public double ChangeAmount { get; set; }
        
        public string HostName { get; set; }
        public string TerminalNumber { get; set; }

        public List<SaleDetailModel> SaleDetails { get; set; }
        public List<SalePaymentModel> SalePayments { get; set; }

        public DiscountType Discounttype { get; set; }
        public string CompanyCode { get; set; }

        public bool AutoGenerateQueueNumber { get; set; }
        public int QueueNumber { get; set; }
        public bool IsPreOrder { get; set; }
        public bool IsDirectToCashier { get; set; }
        public bool IsFromOrderDetail { get; set; }

        public TransactionType TransactionType { get; set; }

        public string SalesCRPNumber { get; set; }
    }
}
