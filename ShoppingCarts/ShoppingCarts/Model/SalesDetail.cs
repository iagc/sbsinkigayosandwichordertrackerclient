﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public partial class SalesDetail
    {
        public int TblIndex { get; set; }
        public string InvoiceNumber { get; set; }
        public string Barcode { get; set; }
        public string FullDescription { get; set; }
        public double OrigPrice { get; set; }
        public double SalePrice { get; set; }
        public double PercentDiscount { get; set; }
        public double IncDiscount { get; set; }
        public double OverridingDiscount { get; set; }
        public System.DateTime SalesDate { get; set; }
        public DateTime? SalesTime { get; set; }
        public string PriceGroup { get; set; }
        public int Quantity { get; set; }
        public int? ReturnQty { get; set; }
        public string DiscountedBy { get; set; }
        public string UOM { get; set; }
        public double DiscountAmount { get; set; }
        public double xSubTotal { get; set; }
        public string Location { get; set; }
        public string Remarks { get; set; }
        public string CRPNumber { get; set; }
        public string StoreCode { get; set; }
        public sbyte Isvoid { get; set; }
        public string ReturnsRefNumber { get; set; }
        public double Cost { get; set; }
        public double MinDiscount { get; set; }
        public double MaxDiscount { get; set; }
        public string CustomerPONo { get; set; }
        public string PredefinedDisc { get; set; }
        public double DiscountedPrice { get; set; }
        public string DiscountSeries { get; set; }
        public double? AmountDiscount { get; set; }
        public double? SurCharge { get; set; }
        public sbyte? AllowCardPrice { get; set; }
        public string ApprovedBy { get; set; }
        public sbyte? SupplierDiscount { get; set; }
        public double? CASH { get; set; }
        public double? CREDITCARD { get; set; }
        public int ItemPointer { get; set; }
        public string ApprovedByCost { get; set; }
        public string ApprovedByOver { get; set; }
        public bool? Migrated { get; set; }
        public bool? Package { get; set; }
        public string ParentBarcode { get; set; }
        public double? xSubTotal2 { get; set; }
        public bool? Vatable { get; set; }
        public string Lotnumber { get; set; }
        public bool? Uploaded { get; set; }
        public bool? Deducted { get; set; }
        public double? CommRate { get; set; }
        public string DiscountCode { get; set; }
        public string DiscountType { get; set; }
        public string DiscountRef { get; set; }
        public bool? Modifier { get; set; }
        public double? FDIscPerItem { get; set; }
        public double? NetPerItem { get; set; }
        public double? SCPerItem { get; set; }
    }
}
