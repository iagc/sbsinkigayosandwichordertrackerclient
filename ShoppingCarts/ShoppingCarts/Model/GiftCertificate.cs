﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public class GiftCertificate
    {
        public int? tblIndex { get; set; }
        public string GCNumber { get; set; }
        public decimal? GCValue { get; set; }
        public sbyte? Status { get; set; }
        public sbyte? ActiveFlag { get; set; }
        public DateTime? ValidityFrom { get; set; }
        public DateTime? ValidityTo { get; set; }
        public string Description { get; set; }
        public System.DateTime LogDateTime { get; set; }
        public bool? GCType { get; set; }
        public bool? Uploaded { get; set; }
        public string PreparedBy { get; set; }
        public string IssuedTo { get; set; }
        public bool? Written { get; set; }
        public string IssuedToCustomer { get; set; }
        public bool? MultiUse { get; set; }
        public double? CurrentValue { get; set; }
    }
}
