﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public partial class Terminal
    {
        public int tbIndex { get; set; }
        public string TerminalNumber { get; set; }
        public string HostName { get; set; }
        public string StoreCode { get; set; }
        public bool Allow_Receipt_Printing { get; set; }
        public bool Allow_Pop_Drawer { get; set; }
        public bool Allow_Refund { get; set; }
        public bool Enable_Clock { get; set; }
        public bool Enable_Reports { get; set; }
        public bool Enable_Managers_Menu { get; set; }
        public bool Enable_POS { get; set; }
        public bool Allow_Returned { get; set; }
        public string TerminalFooter { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? PromptLotnumber { get; set; }
        public string SerialNumber { get; set; }
        public string PrinterSerialNumber { get; set; }
        public string CutCommand { get; set; }
        public string NetworkPrinter { get; set; }
        public string PopUpDrawerCommand { get; set; }
        public string Location { get; set; }
        public System.DateTime LogDateTime { get; set; }
        public string BIRAccreditation { get; set; }
        public string VAT_Account { get; set; }
        public string ThemeName { get; set; }
        public double? New_Grand_Total { get; set; }
        public double? Old_Grand_Total { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string MachineAccreditation { get; set; }
        public bool? AllowPopupCRP { get; set; }
        public bool? AllowPromptForBagger { get; set; }
        public string PrinterName { get; set; }
        public string PrinterPort { get; set; }
        public string InvoiceNumber { get; set; }
        public int? TransactionNumber { get; set; }
        public string PriceDept { get; set; }
        public bool? PerCustomer { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? ComputerDate { get; set; }
        public long? PulloutNumber { get; set; }
        public string ReceiptHeader { get; set; }
        public string SMID { get; set; }
        public long? OrderNumber { get; set; }
        public int? InvoiceNumberExceedCounter { get; set; }
        public string CustomerPoleDisplayPort { get; set; }
    }
}
