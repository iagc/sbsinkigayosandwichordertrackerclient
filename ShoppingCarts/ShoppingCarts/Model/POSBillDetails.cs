﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using MvvmHelpers;

#region Old Code
//namespace ShoppingCarts.Model
//{
//    public class POSBillDetails : INotifyPropertyChanged
//    {
//        private double denomination;
//        private int quantity;
//        private double subtotal;

//        public double Denomination
//        {
//            get { return denomination; }
//            set
//            {
//                this.denomination = value;
//                RaisePropertyChanged("Denomination");
//            }

//        }

//        public int Quantity
//        {
//            get { return quantity; }
//            set
//            {
//                this.quantity = value;
//                RaisePropertyChanged("quantity");
//            }

//        }

//        public double SubTotal
//        {
//            get { return subtotal; }
//            set
//            {
//                this.subtotal = value;
//                RaisePropertyChanged("subtotal");
//            }

//        }
//        //public double Denomination { get; set; }
//        //public int Pieces { get; set; }

//public POSBillDetails(double denomination, int quantity, double subtotal)
//{
//    this.Denomination = denomination;
//    this.Quantity = quantity;
//    this.SubTotal = subtotal;
//}

//        public event PropertyChangedEventHandler PropertyChanged;
//        private void RaisePropertyChanged(string name)
//        {
//            if (this.PropertyChanged != null)
//                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
//        }
//    }


//}

#endregion
namespace ShoppingCarts.Model
{
    public class PosBillDetails : ObservableObject
    {
        private double _denomination;
        public double Denomination { get => _denomination;
            set
            {
                _denomination = value;
                Subtotal = _denomination * _qty;
                OnPropertyChanged("Denomination");
            }
        }

        private int _qty;
        public int Qty { get => _qty;
            set
            {
                _qty = value;
                Subtotal = _denomination * _qty;
                OnPropertyChanged("Qty");
            }
        }

        private double _subTotal;
        public double Subtotal { get => _subTotal;
            set
            {
                _subTotal = value;
                OnPropertyChanged("Subtotal");
            }
        }
        
        public PosBillDetails(double denomination, int quantity)
        {
            this.Denomination = denomination;
            this.Qty = quantity;
        }
    }

    
}
