﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public class TransactionType
    {
        public string TransactionCode { get; set; }
        public string TransactionDescription { get; set; }
        public bool RequiresCustomerInfo { get; set; }
        public bool AutoCompletePayment { get; set; }
        public Int64 PaymentType { get; set; }
        public bool DiscountOption { get; set; }
        public string ReferenceCode { get; set; }
    }
}