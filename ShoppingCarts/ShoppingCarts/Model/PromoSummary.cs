﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public class PromoSummary
    {
        public string PromoID { get; set; }
        public string PromoDescription { get; set; }
        public bool UsesCoupon { get; set; }
        public List<ProductDetail> PromoItems { get; set; }
    }
}
