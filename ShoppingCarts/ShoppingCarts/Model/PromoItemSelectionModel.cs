﻿using System;
using System.Collections.Generic;
using System.Text;
using ShoppingCarts.Model;

namespace ShoppingCarts.Model
{
    public class PromoItemSelectionModel
    {
        public int id { get; set; }
        public string bundlename { get; set; }
        public bool required { get; set; }
        public int totalqty { get; set; }
        public IList<ProductDetail> PromoItems { get; set; }
        public string namesort
        {
            get
            {
                if (string.IsNullOrWhiteSpace(bundlename) || bundlename.Length == 0)
                    return "?";

                return bundlename.ToString().ToUpper();
            }
        }
    }
}
