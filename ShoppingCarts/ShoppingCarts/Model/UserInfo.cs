﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public class UserInfo
    {
        public string employeenumber { get; set; }
        public string employeeFirstname { get; set; }
        public string employeeLastname { get; set; }
        public string employeeStoreCode { get; set; }
        public string employeePosition { get; set; }
        public string EmployeeDepartment { get; set; }


    }
}
