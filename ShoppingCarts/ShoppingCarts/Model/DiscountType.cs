﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public class DiscountType
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public double Percent { get; set; }
        public bool UserDefine { get; set; }
        public int IsDeleted { get; set; }
        public bool DiscountMode { get; set; }
        public bool Ticket { get; set; }

        public double DiscountAmount { get; set; } = 0;
        public string CustomerID { get; set; } = "";
        public string CustomerName { get; set; } = "";
    }
}