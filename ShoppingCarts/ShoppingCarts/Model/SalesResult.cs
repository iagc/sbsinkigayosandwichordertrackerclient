﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public class SalesResult
    {
        public string SalesInvoiceNumber { get; set; }
        public string QueueNumber { get; set; }
        public double ChangeAmount { get; set; }
    }
}
