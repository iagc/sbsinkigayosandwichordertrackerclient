﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public partial class SalesCustomerInfo
    {
        public string SalesInvoiceNumber { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerContactNumber { get; set; }
        public string CustomerContact { get; set; }
        public string CustomerEmailAddress { get; set; }
        public string CustomerTIN { get; set; }
        public string CustomerID { get; set; }
        public byte Uploaded { get; set; }
        public string CustomerPD { get; set; }
        public string Remarks { get; set; }
        public string BusinessStyle { get; set; }
    }

}
