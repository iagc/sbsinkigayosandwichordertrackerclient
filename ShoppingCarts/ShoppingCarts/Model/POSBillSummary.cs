﻿using System;
using System.Collections.Generic;
using System.Text;
using MvvmHelpers;

namespace ShoppingCarts.Model
{
    public class PosBillSummary
    {
        public string EmployeeNumber { get; set; }
        public string StoreCode { get; set; }
        public string HostName { get; set; }
        public string TerminalNumber { get; set; }

        public List<PosBillDetails> PosBillAppDetails { get; set; }
        public PosBillSummary()
        {
        }

        public PosBillSummary(string employeeNumber,string storeCode, string hostname, string terminalNumber, List<PosBillDetails> posBillDetails)
        {
            this.EmployeeNumber = employeeNumber;
            this.StoreCode = storeCode;
            this.HostName = hostname;
            this.TerminalNumber = terminalNumber;
            this.PosBillAppDetails = posBillDetails;
        }
    }
}
