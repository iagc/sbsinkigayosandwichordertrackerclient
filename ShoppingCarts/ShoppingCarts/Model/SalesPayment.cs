﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    public partial class SalesPayment
    {
        public int TblIndex { get; set; }
        public string InvoiceNumber { get; set; }
        public int PaymentType { get; set; }
        public string Description { get; set; }
        public double? Amount { get; set; }
        public string GCNumber { get; set; }
        public string CreditMemoNumber { get; set; }
        public string CardNumber { get; set; }
        public string CardName { get; set; }
        public string FullName { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string ApprovalCode { get; set; }
        public string BankName { get; set; }
        public string DepositTo { get; set; }
        public DateTime? DateDeposited { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime? CardDate { get; set; }
        public string Comment1 { get; set; }
        public string Comment2 { get; set; }
        public string Comment3 { get; set; }
        public sbyte? IsPostDated { get; set; }
        public sbyte? IsTerms { get; set; }
        public string Terms { get; set; }
        public string StoreCode { get; set; }
        public string DRNumber { get; set; }
        public int? CustomerPONo { get; set; }
        public sbyte? IsPartial { get; set; }
        public string EPBAccountNo { get; set; }
        public DateTime? EPBDateDeposit { get; set; }
        public string EPBReferenceNo { get; set; }
        public string EPBCustomerBank { get; set; }
        public string EPBDepositToWhatBank { get; set; }
        public double? FDiscountAmount { get; set; }
        public int? CheckStatus { get; set; }
        public double BankCharges { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string ORNumber { get; set; }
        public bool? Migrated { get; set; }
        public bool? CollectionType { get; set; }
        public double? VatCharges { get; set; }
        public bool? Uploaded { get; set; }
    }
}
