﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingCarts.Model
{
    [Serializable]
    public class Voucher
    {
        public string VoucherCode { get; set; } = "";
        public int UsageLimit { get; set; } = 0;
    }
}
