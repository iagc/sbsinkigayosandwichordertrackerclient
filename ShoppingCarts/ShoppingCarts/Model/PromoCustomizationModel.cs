﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace ShoppingCarts.Model
{
    public class PromoCustomizationModel : INotifyPropertyChanged
    {
        private int id1;
        private string bundlename1;
        private bool required1;
        private int totalqty1;
        private string barcode;
        private string category;
        private string fullDescription;
        private string shortDescription;
        private double origPrice;
        private double price;
        private int status;
        private int quantity = 1;
        private double subtotal = 0.00;
        private string promoID = "";
        private string promoDescription = "";

        // boiler-plate
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        public int id { get => id1; set { SetField(ref id1, value, "id"); } }
        public string bundlename { get => bundlename1; set { SetField(ref bundlename1, value, "bundlename"); } }
        public bool required { get => required1; set { SetField(ref required1, value, "required"); } }
        public int totalqty { get => totalqty1; set { SetField(ref totalqty1, value, "totalqty"); } }
        public string Barcode { get => barcode; set { SetField(ref barcode, value, "Barcode"); } }
        public string Category { get => category; set { SetField(ref category, value, "Category"); } }
        public string FullDescription { get => fullDescription; set { SetField(ref fullDescription, value, "FullDescription"); } }
        public string ShortDescription { get => shortDescription; set { SetField(ref shortDescription, value, "ShortDescription"); } }
        public double OrigPrice { get => origPrice; set { SetField(ref origPrice, value, "OrigPrice"); } }
        public double Price { get => price; set { SetField(ref price, value, "Price"); } }
        public int Status { get => status; set { SetField(ref status, value, "Status"); } }
        public int Quantity { get => quantity; set { SetField(ref quantity, value, "Quantity"); } }
        public double Subtotal { get => subtotal; set { SetField(ref subtotal, value, "Subtotal"); } }
        public string PromoID { get => promoID; set { SetField(ref promoID, value, "PromoID"); } }
        public string PromoDescription { get => promoDescription; set { SetField(ref promoDescription, value, "PromoDescription"); } }
        public Voucher Voucher { get; set; } = new Voucher();
        public string GetName() {
            return bundlename + " Please Select " + totalqty;
        }
    }
}
